import { toLocaleDate } from '../public/js/toLocaleDate'

describe('toLocaleDate', () => {
  it('retrieves no date symbol for an empty date', () => {
    const emptyDate = ''

    const noDateSymbol = toLocaleDate(emptyDate)

    expect(noDateSymbol).toEqual('--/--/--')
  })

  it('retrieves form date with locale format', () => {
    const formDate = '2000-10-20'

    const localeDate = toLocaleDate(formDate)

    expect(localeDate).toEqual('10/20/2000')
  })
})
