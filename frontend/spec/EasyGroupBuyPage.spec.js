import { TestEasyGroupBuyPage } from './support/TestEasyGroupBuyPage'
import { TestUser } from './support/TestUser'

describe('EasyGroupBuyPage', () => {
  it('shows BuyGroups detail', () => {

    const page = new TestEasyGroupBuyPage().draw()

    expect(page.showsDetail()).toEqual(true)
  })

  it('may change to another detail', () => {
    const page = new TestEasyGroupBuyPage().draw()

    page.chooseAnotherBuyGroup()

    expect(page.showsAnotherDetail()).toEqual(true)
  })

  it('shows BuyGroup list', () => {

    const page = new TestEasyGroupBuyPage().draw()

    expect(page.showsList()).toEqual(true)
  })

  it('shows BuyGroup creation', () => {
    const page = new TestEasyGroupBuyPage().draw()

    page.clickAdd()

    expect(page.showsCreationModal()).toEqual(true)
  })

  it('shows created BuyGroup', () => {
    const newBuyGroup = { title: 'newTitle', description: 'newDescription', requirements: 'newRequirements' }
    const page = new TestEasyGroupBuyPage().draw()

    page.clickAdd()
    page.fillNew(newBuyGroup)
    page.create()

    expect(page.showsInList(newBuyGroup)).toEqual(true)
    expect(page.showsInDetail(newBuyGroup)).toEqual(true)
  })

  it('can request to join to BuyGroup', () => {
    const page = new TestEasyGroupBuyPage().draw()

    page.clickRequestToJoin()

    expect(page.showsRequestModal()).toEqual(true)
  })

  it('shows requesters below detail', () => {
    const requiredInformation = 'someRequiredInformation'
    const page = new TestEasyGroupBuyPage().draw()

    page.clickRequestToJoin()
    page.fillRequirements(requiredInformation)
    page.askToJoin()

    expect(page.includes(requiredInformation)).toEqual(true)
  })

  it('becomes a request to participation', () => {
    const requiredInformation = 'someRequiredInformation'
    const page = new TestEasyGroupBuyPage().draw()
    page.createRequest(requiredInformation)

    page.acceptAsParticipant()
    page.showParticipations()

    expect(page.includes(requiredInformation)).toEqual(true)
    expect(page.hasAParticipation()).toEqual(true)
  })

  it('can not request to join twice', () => {
    const page = new TestEasyGroupBuyPage().draw()

    page.createRequest('someRequiredInformation')

    expect(page.hasRequestToJoinDisabled()).toEqual(true)
  })

  it('can edit a BuyGroup', () => {
    const page = new TestEasyGroupBuyPage().draw()

    page.clickEdit()

    expect(page.showsCreationModal()).toEqual(true)
  })

  it('comments requests', () => {
    const commentText = 'someCommentText'
    const page = new TestEasyGroupBuyPage().draw()
    page.createRequest('someRequiredInformation')

    page.fillComment(commentText)
    page.comment()

    expect(page.includes(commentText)).toEqual(true)
  })

  it('can collapse involveds', () => {
    const commentText = 'someCommentText'
    const page = new TestEasyGroupBuyPage().draw()
    page.createComment(commentText)

    page.collapseRequest()

    expect(page.includes(commentText)).toEqual(false)
  })

  it('can collapse involveds', () => {
    const commentText = 'someCommentText'
    const page = new TestEasyGroupBuyPage().draw()
    page.createComment(commentText)
    page.collapseRequest()

    page.expandRequest()

    expect(page.includes(commentText)).toEqual(true)
  })

  it('moves to login for unidentified users when tries to create a BuyGroup', () => {
    const page = new TestEasyGroupBuyPage({ user: TestUser.unidentified() }).draw()

    page.clickAdd()

    expect(page.movedToLogin()).toEqual(true)
    expect(page.showsCreationModal()).toEqual(false)
  })

  it('can reject a participant', () => {
    const information = 'someRequiredInformation'
    const page = new TestEasyGroupBuyPage().draw()
    page.createParticipation(information)

    page.rejectParticipant()

    expect(page.includes(information)).toEqual(false)
    expect(page.includes('6/10')).toEqual(false)
  })

  it('shows a rejected message intead participation when is rejected', () => {
    const page = new TestEasyGroupBuyPage().draw()
    page.createParticipation('someRequiredInformation')

    page.rejectParticipant()

    page.showParticipations()
    expect(page.includes('Este participante a sido expulsado')).toEqual(true)
  })

  it('updates the quantity of participants in list', () => {
    const page = new TestEasyGroupBuyPage().draw()

    page.createParticipation('someRequiredInformation')

    expect(page.includes('5/10')).toEqual(false)
  })
})
