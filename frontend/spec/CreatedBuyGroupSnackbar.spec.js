import { CreatedBuyGroupSnackbar } from "../public/js/CreatedBuyGroupSnackbar"
import { SpyDocument } from "./support/SpyDocument"

describe('CreatedBuyGroupSnackbar', () => {
  let spyDocument

  beforeEach(() => {
    spyDocument = new SpyDocument()
  })

  it('adds a text to snackbar element', () => {

    new CreatedBuyGroupSnackbar()

    expect(spyDocument.text()).toEqual('Terminado!')
  })

  it('shows a snackbar element', () => {
    const snackbar = new CreatedBuyGroupSnackbar(spyDocument)

    snackbar.show()

    expect(spyDocument.getClassesById('snackbar')).toEqual('show')
  })

  it('hides a snackbar element', () => {
    const notShow = ''
    const snackbar = new CreatedBuyGroupSnackbar(spyDocument)
    snackbar.show()

    snackbar.hideAfterTime()

    expect(spyDocument.getClassesById('snackbar')).toEqual(notShow)
  })
})
