import { StubLocalStorage } from '../support/StubLocalStorage'
import { StubWindow } from '../support/StubWindow'
import { User } from '../../public/js/domain/User'

describe('User', () => {
  let window

  beforeEach(() => {
    window = new StubWindow()
  })

  afterEach(() => {
    StubLocalStorage.restore()
    window.restore()
  })

  it('gives real login url for no development', () => {
    window.moveToRealUrl()

    const loginUrl = User.loginUrl()

    expect(loginUrl).toEqual('login.html')
  })

  it('gives fake login url for development', () => {
    window.moveToDevelopmentUrl()

    const loginUrl = User.loginUrl()

    expect(loginUrl).toEqual('fake_login.html')
  })

  it('retrieves data from localstorage', () => {
    const userData = { userToken: 'someThing', userPhotoUrl: 'image', userUsername: 'name' }
    new StubLocalStorage(userData)

    const user = User

    expect(user.name()).toEqual(userData.userUsername)
    expect(user.image()).toEqual(userData.userPhotoUrl)
    expect(user.isLogged()).toEqual(true)
  })

  it('can move to login if the user is unidentified', () => {
    window.moveToDevelopmentUrl()
    new StubLocalStorage({})

    User.moveToLoginIfIsUnidentified()

    const movedUrl = global.window.location.href
    expect(movedUrl).toEqual('fake_login.html')
  })

  it('can move to login', () => {
    window.moveToDevelopmentUrl()
    new StubLocalStorage({})

    User.moveToLogin()

    const movedUrl = global.window.location.href
    expect(movedUrl).toEqual('fake_login.html')
  })
})
