import { BuyGroupSamples } from "../support/BuyGroupSamples"
import { BuyGroup } from "../../public/js/domain/BuyGroup"

describe('BuyGroup', () => {
  it('builds from a form data', () => {
    const newBuyGroup = BuyGroupSamples.newBuyGroupFromForm()
    const formData = BuyGroupSamples.buyGroupFromForm()
    const id = 1

    const buyGroup = BuyGroup.fromForm(formData, id)

    expect(buyGroup.toDictionary()).toEqual(newBuyGroup)
  })

  it('increases the current quantity of participants', () => {
    const rawBuyGroup = BuyGroupSamples.some()
    rawBuyGroup.currentParticipants = 0
    rawBuyGroup.maximumParticipants = 1
    const buyGroup = new BuyGroup(rawBuyGroup)

    buyGroup.increaseCurrentParticipantsByOne()

    expect(buyGroup.toDictionary().currentParticipants).toEqual(1)
    expect(buyGroup.toDictionary().status).toEqual('CLOSED')
  })

  it('decreases the current quantity of participants', () => {
    const rawBuyGroup = BuyGroupSamples.some()
    rawBuyGroup.currentParticipants = 0
    rawBuyGroup.maximumParticipants = 1
    const buyGroup = new BuyGroup(rawBuyGroup)
    buyGroup.increaseCurrentParticipantsByOne()

    buyGroup.decreaseCurrentParticipantsByOne()

    expect(buyGroup.toDictionary().currentParticipants).toEqual(0)
    expect(buyGroup.toDictionary().status).toEqual('OPEN')
  })
})
