import { TestRequestBuilder } from '../support/TestRequestBuilder'
import { BuyGroupSamples } from '../support/BuyGroupSamples'
import { Actions } from '../../public/js/domain/Actions'
import { Collection } from '../../public/js/Collection'


describe('Actions', () => {
  let samples

  beforeEach(() => {
    samples = BuyGroupSamples.many()
  })

  it('retrieves Buy Groups', () => {
    const collection = new Collection(samples)

    const buyGroups = new Actions(collection).retrieveBuyGroups()

    expect(buyGroups).toEqual(samples)
  })

  it('creates a BuyGroup', () => {
    const collection = new Collection()

    const createdBuyGroup = new Actions(collection).createBuyGroups(buyGroupFromForm())

    const buyGroups = new Actions(collection).retrieveBuyGroups()
    expect(createdBuyGroup).toEqual(newBuyGroup())
    expect(buyGroups).toEqual([createdBuyGroup])
  })

  it('request to join to BuyGroup', () => {
    const collection = new Collection()
    const requestFromForm = { information: 'someRequirements' }

    const createdRequest = new Actions(collection).requestToJoin('buyGroupId', requestFromForm)

    const requests = new Actions(collection).retrieveRequests('buyGroupId')
    expect(createdRequest).toEqual(newRequest())
    expect(requests).toEqual([createdRequest])
  })

  it('becomes requests to participations', () => {
    const buyGroupId = 1
    const collection = new Collection(samples)
    const actions = new Actions(collection)
    const requestFromForm = { information: 'someRequirements' }
    const createdRequest = actions.requestToJoin(buyGroupId, requestFromForm)

    const participantion = actions.becomeParticipation(createdRequest)

    const requests = actions.retrieveRequests(buyGroupId)
    const participantions = actions.retrieveParticipations(buyGroupId)
    expect(requests).toEqual([])
    expect(participantion.id).toEqual(createdRequest.id)
    expect(participantions).toEqual([participantion])
  })

  it('increases the quantity of participants when becomes a participant', () =>{
    const collection = new Collection(samples)
    const actions = new Actions(collection)
    const buyGroup = samples[0]
    const requestFromForm = { information: 'someRequirements' }
    const createdRequest = actions.requestToJoin(buyGroup.id, requestFromForm)
    actions.becomeParticipation(createdRequest)

    expect(buyGroup.currentParticipants).toEqual(2)
  })

  it('updates the BuyGroup detail', () => {
    const collection = new Collection(samples)
    const actions = new Actions(collection)
    const buyGroup = actions.retrieveBuyGroups()[0]
    const newTitle = 'New Title'
    buyGroup.title = newTitle

    actions.updateBuyGroup(buyGroup)

    const updated = actions.retrieveBuyGroups()[0]
    expect(updated.title).toEqual(newTitle)
  })

  it('can comment requests', () => {
    const collection = new Collection()
    const actions = new Actions(collection)
    const createdRequest = actions.requestToJoin('buyGroupId', { information: 'someRequirements' })

    const commentedRequest = actions.comment(createdRequest, { comment: 'anyComment' })

    const requests = actions.retrieveRequests('buyGroupId')
    expect(commentedRequest).toEqual(expectedCommentedRequest())
    expect(requests).toEqual([commentedRequest])
  })

  it('can comment commented requests', () => {
    const collection = new Collection()
    const actions = new Actions(collection)
    const createdRequest = actions.requestToJoin('buyGroupId', { information: 'someRequirements' })
    actions.comment(createdRequest, { comment: 'anyComment' })

    const commentedRequest = actions.comment(createdRequest, { comment: 'anotherComment' })

    const requests = actions.retrieveRequests('buyGroupId')
    expect(commentedRequest).toEqual(expectedRecommentedRequest())
    expect(requests).toEqual([commentedRequest])
  })

  it('can reject a involved', () => {
    const collection = new Collection()
    const actions = new Actions(collection)
    const createdBuyGroup = actions.createBuyGroups(buyGroupFromForm())
    const request = actions.requestToJoin(createdBuyGroup.id, { information: 'someRequirements' })

    actions.rejectInvolved(request)

    const rejectedInvolved = actions.retrieveRequests(request.buyGroupId)[0]
    expect(rejectedInvolved.id).toEqual(request.id)
    expect(rejectedInvolved.isRejected).toEqual(true)
    const buyGroup = actions.retrieveBuyGroups()[0]
    expect(buyGroup.currentParticipants).toEqual(0)
  })

  it('updates the quantity of participants when rejects a participant', () => {
    const collection = new Collection()
    const actions = new Actions(collection)
    const createdBuyGroup = actions.createBuyGroups(buyGroupFromForm())
    const request = actions.requestToJoin(createdBuyGroup.id, { information: 'someRequirements' })
    const participation = actions.becomeParticipation(request)

    actions.rejectInvolved(participation)

    const buyGroup = actions.retrieveBuyGroups()[0]
    expect(buyGroup.currentParticipants).toEqual(0)
  })

  it('updates the BuyGroup status when reach maximum quantity of participants', () => {
    const collection = new Collection()
    const actions = new Actions(collection)
    const createdBuyGroup = actions.createBuyGroups(buyGroupWithOneMaximumParticipant())
    const request = actions.requestToJoin(createdBuyGroup.id, { information: 'someRequirements' })

    actions.becomeParticipation(request)

    const buyGroup = actions.retrieveBuyGroups()[0]
    expect(buyGroup.status).toEqual('CLOSED')
  })

  it('updates the BuyGroup status when does not reach maximum quantity of participants', () => {
    const collection = new Collection()
    const actions = new Actions(collection)
    const createdBuyGroup = actions.createBuyGroups(buyGroupWithOneMaximumParticipant())
    const request = actions.requestToJoin(createdBuyGroup.id, { information: 'someRequirements' })
    const participation = actions.becomeParticipation(request)

    actions.rejectInvolved(participation)

    const buyGroup = actions.retrieveBuyGroups()[0]
    expect(buyGroup.status).toEqual('OPEN')
  })

  function newBuyGroup() {
    return BuyGroupSamples.newBuyGroupFromForm()
  }

  function buyGroupFromForm() {
    return BuyGroupSamples.buyGroupFromForm()
  }

  function buyGroupWithOneMaximumParticipant() {
    return BuyGroupSamples.buyGroupWithOneMaximumParticipant()
  }

  function expectedCommentedRequest() {
    return new TestRequestBuilder().withOneComment().build()
  }

  function expectedRecommentedRequest() {
    return new TestRequestBuilder().withTwoComments().build()
  }

  function newRequest() {
    return TestRequestBuilder.default()
  }
})
