import { TestLogin } from "../../support/testComponets/TestLogin"
import { TestUser } from "../../support/TestUser"

describe('Login', () => {
  it('shows login button for unidentified user', () => {
    const data = { user: TestUser.unidentified() }

    const login = new TestLogin(data)

    expect(login.hasButton()).toEqual(true)
  })

  it('shows avatar for logged user', () => {
    const data = { user: TestUser.logged() }

    const login = new TestLogin(data)

    expect(login.hasAvatar()).toEqual(true)
    expect(login.hasButton()).toEqual(false)
  })
})
