import { Involved } from "../../../public/js/components/buyGroupDetail/Involved"

describe('Involved', () => {
  it('shows the are not message when there are no involved', () => {
    const noInvolved = []
    const retriveInvolved = () => { return noInvolved }
    const groupName = 'requests'

    const template = new Involved(retriveInvolved, groupName).template({ hasToShow: true })

    expect(template).toContain('No hay')
  })
})
