import { TestBuyGroupDetail } from '../../support/testComponets/TestBuyGroupDetail'
import { BuyGroupSamples } from '../../support/BuyGroupSamples'
import { TestCallback } from '../../support/TestCallback'

describe('BuyGroupDetail', () => {
  let firstRequest, lastRequest, firstParticipation, lastParticipation
  const buyGroup = BuyGroupSamples.some()

  beforeEach(() => {
    firstRequest = { information: 'someRequiredInformation' }
    lastRequest = { information: 'lastRequiredInformation' }
    firstParticipation = { information: 'someRequiredInformation' }
    lastParticipation = { information: 'lastRequiredInformation' }
  })

  it('draws detailed info', () => {
    const data = { buyGroup: buyGroup, retrieveRequests: stubActionWith([]), retrieveParticipations: stubActionWith([]) }

    const detail = new TestBuyGroupDetail(data).draw()

    expect(detail.showsDetail(buyGroup)).toEqual(true)
    expect(detail.showsInvolvedTabs()).toEqual(false)
  })

  it('executes a callback when request for join', () => {
    const requestToJoin = new TestCallback()
    const data = { buyGroup: buyGroup, requestToJoin: requestToJoin.execute, retrieveRequests: stubActionWith([]), retrieveParticipations: stubActionWith([]) }
    const detail = new TestBuyGroupDetail(data).draw()

    detail.requestToJoin()

    expect(requestToJoin.hasBeenCalled()).toEqual(true)
  })

  it('shows all the requests', () => {
    const data = { buyGroup: buyGroup, retrieveRequests: stubActionWith([firstRequest, lastRequest]), retrieveParticipations: stubActionWith([]) }

    const detail = new TestBuyGroupDetail(data).draw()

    expect(detail.firstRequest()).toContain(lastRequest.information)
    expect(detail.secondRequest()).toContain(firstRequest.information)
    expect(detail.isActiveParticipationsTab()).toEqual(false)
    expect(detail.isActiveRequestsTab()).toEqual(true)
  })

  it('shows all the participations', () => {
    const data = { buyGroup: buyGroup, retrieveParticipations: stubActionWith([firstParticipation, lastParticipation]), retrieveRequests: stubActionWith([firstRequest]) }
    const detail = new TestBuyGroupDetail(data).draw()

    detail.showParticipations()

    expect(detail.firstParticipation()).toContain(lastParticipation.information)
    expect(detail.secondParticipation()).toContain(firstParticipation.information)
    expect(detail.firstRequest()).not.toContain(firstRequest.information)
    expect(detail.isActiveParticipationsTab()).toEqual(true)
    expect(detail.isActiveRequestsTab()).toEqual(false)
    expect(detail.canAcceptAParticipation()).toEqual(false)
  })

  it('can returns to show the requests list', () => {
    const data = { buyGroup: buyGroup, retrieveParticipations: stubActionWith([firstParticipation]), retrieveRequests: stubActionWith([firstRequest]) }
    const detail = new TestBuyGroupDetail(data).draw()
    detail.showParticipations()

    detail.showRequests()

    expect(detail.firstParticipation()).not.toContain(firstParticipation.information)
    expect(detail.firstRequest()).toContain(firstRequest.information)
    expect(detail.isActiveParticipationsTab()).toEqual(false)
    expect(detail.isActiveRequestsTab()).toEqual(true)
  })

  it('shows involved tabs when only has participations', () => {
    const data = { buyGroup: buyGroup, retrieveParticipations: stubActionWith([firstParticipation]), retrieveRequests: stubActionWith([]) }
    const detail = new TestBuyGroupDetail(data).draw()
    detail.showParticipations()

    detail.showRequests()

    expect(detail.firstParticipation()).not.toContain(firstParticipation.information)
    expect(detail.showsInvolvedTabs()).toEqual(true)
  })

  it('can transform a request to participation', () => {
    const becomeParticipation = new TestCallback()
    firstRequest.id = 0
    const data = { buyGroup: buyGroup, becomeParticipation: becomeParticipation.execute, retrieveParticipations: stubActionWith([]), retrieveRequests: stubActionWith([firstRequest]), reload: function() {} }
    const detail = new TestBuyGroupDetail(data).draw()

    detail.acceptRequest()

    expect(becomeParticipation.hasBeenCalledWith()).toEqual(firstRequest)
  })

  function stubActionWith(collection) {
    return () => { return collection }
  }
})
