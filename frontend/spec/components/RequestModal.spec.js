import { TestRequestModal } from '../support/testComponets/TestRequestModal'
import { BuyGroupSamples } from '../support/BuyGroupSamples'
import { TestCallback } from '../support/TestCallback'

describe('RequestModal', () => {
  it('starts hidden', () => {

    const modal = new TestRequestModal({})

    expect(modal.isHidden()).toEqual(true)
  })

  it('can be shown', () => {
    const modal = new TestRequestModal({})

    modal.show()

    expect(modal.isHidden()).toEqual(false)
    expect(modal.isRequirementsFocused()).toEqual(true)
  })

  it('can be closed', () => {
    const modal = new TestRequestModal({})
    modal.show()

    modal.close()

    expect(modal.isHidden()).toEqual(true)
  })

  it('can be canceled', () => {
    const modal = new TestRequestModal({})
    modal.show()

    modal.cancel()

    expect(modal.isHidden()).toEqual(true)
  })

  it('shows BuyGroup requirements', () => {
    const buyGroup = BuyGroupSamples.some()
    const data = { requirements: buyGroup.requirements }

    const modal = new TestRequestModal(data).show()

    expect(modal.text()).toContain(buyGroup.requirements)
  })

  it('has request button disabled', () => {

    const modal = new TestRequestModal({}).show()

    expect(modal.hasRequestButtonDisabled()).toEqual(true)
  })

  it('enables request when fills requirements', () => {
    const modal = new TestRequestModal({}).show()

    modal.fill('requirements')

    expect(modal.hasRequestButtonDisabled()).toEqual(false)
  })

  it('can request to join', () => {
    const requirements = 'someRequirements'
    const requestJoinCallback = new TestCallback()
    const modal = new TestRequestModal({ requestToJoin: requestJoinCallback.execute }).show()

    modal.fill(requirements)
    modal.requestJoin()

    expect(requestJoinCallback.hasBeenCalledWith()).toEqual(requirements)
    expect(modal.isHidden()).toEqual(true)
  })
})
