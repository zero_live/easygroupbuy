import { TestBuyGroupModal } from '../support/testComponets/TestBuyGroupModal'
import { TestCallback } from '../support/TestCallback'
import { TestUser } from '../support/TestUser'

describe('BuyGroupModal', () => {
  it('starts hidden', () => {

    const modal = new TestBuyGroupModal()

    expect(modal.isHidden()).toEqual(true)
  })

  it('can be shown', () => {
    const modal = new TestBuyGroupModal({ user: TestUser.logged() })

    modal.show()

    expect(modal.isHidden()).toEqual(false)
    expect(modal.isTitleFocused()).toEqual(true)
  })

  it('can be hide', () => {
    const modal = new TestBuyGroupModal({ user: TestUser.logged() })
    modal.show()

    modal.hide()

    expect(modal.isHidden()).toEqual(true)
  })

  it('can create a BuyGroup', () => {
    const newBuyGroup = { some: 'BuyGroup' }
    const selectCallback = new TestCallback()
    const reloadCallback = new TestCallback()
    const creationCallback = new TestCallback({ andReturn: newBuyGroup })
    const modal = new TestBuyGroupModal({ create: creationCallback.execute, selectBuyGroup: selectCallback.execute , reload: reloadCallback.execute, user: TestUser.logged() })
    const newBuyGroupData = { title: 'anyTitle', maximumParticipants: 10, finalizationDate: '2222-22-22', description: 'anyDescription', requirements: 'anyRequirement' }
    modal.show()

    modal.fill(newBuyGroupData)
    modal.create()

    expect(creationCallback.hasBeenCalledWith()).toEqual(newBuyGroupData)
    expect(modal.isHidden()).toEqual(true)
    expect(selectCallback.hasBeenCalledWith()).toEqual(newBuyGroup)
    expect(reloadCallback.hasBeenCalled()).toEqual(true)
  })

  it('does not creates a BuyGroup with missing information', () => {
    const callback = new TestCallback()
    const modal = new TestBuyGroupModal({ create: callback.execute, user: TestUser.logged() })
    const invalidBuyGroup = { title: '', maximumParticipants: '', finalizationDate: '', description: '', requirements: '' }
    modal.show()
    modal.fill(invalidBuyGroup)

    modal.create()

    expect(modal.isHidden()).toEqual(false)
    expect(callback.hasBeenCalled()).toEqual(false)
    expect(modal.showsErrors()).toEqual(true)
  })
})
