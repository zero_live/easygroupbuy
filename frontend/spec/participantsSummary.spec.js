import { participantsSummary } from '../public/js/participantsSummary'
import { BuyGroupSamples } from './support/BuyGroupSamples'

describe('participansSummary', () => {
  let buyGroup

  beforeEach(() => {
    buyGroup = BuyGroupSamples.some()
  })

  it('shows the Buy Group participants summary', () => {

    const summary = new TestParticipantSummary({ buyGroup })

    expect(summary.hasCurrentParticipants()).toEqual(true)
    expect(summary.hasMaximumParticipants()).toEqual(true)
    expect(summary.hasSplit()).toEqual(true)
  })

  it('does not show maximum participant when it is not defined', () => {
    buyGroup.maximumParticipants = undefined

    const summary = new TestParticipantSummary({ buyGroup })

    expect(summary.hasMaximumParticipants()).toEqual(false)
    expect(summary.hasSplit()).toEqual(false)
  })
})

class TestParticipantSummary {
  constructor(data) {
    this.buyGroup = data.buyGroup
    this.component = participantsSummary(this.buyGroup)
  }

  hasCurrentParticipants() {
    return this.component.includes('<i class="fas fa-users"></i>' + this.buyGroup.currentParticipants)
  }

  hasMaximumParticipants() {
    return this.component.includes('/'+this.buyGroup.maximumParticipants)
  }

  hasSplit() {
    return this.component.includes(this.buyGroup.currentParticipants+'/')
  }
}
