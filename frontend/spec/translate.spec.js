import { translate } from '../public/js/translate'

describe('translate', () => {
  it('retrieves text for a label', () => {
    const label = 'ebg'

    const text = translate(label)

    expect(text).toEqual('Easy Buy Group')
  })

  it('throws an error for a missing label', () => {

    const act = () => { translate('missing_label') }

    expect(act).toThrow()
  })
})
