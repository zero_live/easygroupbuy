
export class StubLocalStorage {
  static restore() {
    global.localStorage = undefined
  }

  constructor(intialData) {
    this.data = intialData

    global.localStorage = this
  }

  getItem(key) {
    return this.data[key]
  }
}
