
const FAKE_LOGIN_URL = 'fake_login.html'

export class TestUser {
  static logged() {
    return {
      isLogged: function() { return true },
      name: function() { return 'Zero_Live' },
      image: function() { return 'images/128x128.jpg' },
      loginUrl: function() { return FAKE_LOGIN_URL },
      moveToLogin: function() {}
    }
  }

  static unidentified() {
    return new UnidentifiedUser()
  }
}

class UnidentifiedUser {
  constructor() {
    this.hasBeenMovedToLogin = false
  }

  isLogged() {
    return false
  }

  name() {
    return null
  }

  image() {
    return null
  }

  loginUrl() {
    return FAKE_LOGIN_URL
  }

  moveToLogin() {
    this.hasBeenMovedToLogin = true
  }

  movedToLogin() {
    return this.hasBeenMovedToLogin
  }
}
