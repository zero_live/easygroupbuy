
export class StubWindow {
  constructor() {
    this.location = {
      href: ''
    }

    global.window = this
  }

  moveToRealUrl() {
    this.location.href = 'gitlab'
  }

  moveToDevelopmentUrl() {
    this.location.href = 'localhost'
  }

  restore() {
    global.window = undefined
  }
}
