
import { EasyGroupBuyPage } from '../../public/js/EasyGroupBuyPage'
import { Actions } from '../../public/js/domain/Actions'
import { Collection } from '../../public/js/Collection'
import { BuyGroupSamples } from './BuyGroupSamples'
import { SpyDocument } from './SpyDocument'
import { TestUser } from './TestUser'

export class TestEasyGroupBuyPage {
  constructor(data = {}) {
    this.SELECTED_BUY_GROUP = 123
    this.DETAIL_ID = 'buyGroupDetail'

    this.spyDocument = new SpyDocument()
    const collection = new Collection(BuyGroupSamples.many())
    const actions = new Actions(collection)
    this.user = data.user || TestUser.logged()
    this.page = new EasyGroupBuyPage(actions, this.user)
  }

  draw() {
    this.page.draw()

    return this
  }

  clickAdd() {
    this.spyDocument.click('new-buy-group-button')
  }

  clickEdit() {
    this.spyDocument.click('edit-buy-group-button')
  }

  clickRequestToJoin() {
    this.spyDocument.click('request-to-join-button')
  }

  fillRequirements(text) {
    this.spyDocument.input('requirements', text)
  }

  fillNew(buyGroup) {
    this.spyDocument.fill('new-title', buyGroup.title)
    this.spyDocument.fill('new-finalization-date', '')
    this.spyDocument.fill('new-description', buyGroup.description)
    this.spyDocument.fill('new-requirements', buyGroup.requirements)
  }

  fillComment(text) {
    this.spyDocument.fill('0-comment-input', text)
  }

  comment() {
    this.spyDocument.click('0-comment-button')
  }

  collapseRequest() {
    this.spyDocument.click('0-collapse')
  }

  expandRequest() {
    this.collapseRequest()
  }

  createComment(text) {
    this.createRequest('someRequiredInformation')
    this.fillComment(text)
    this.comment()
  }

  create() {
    this.spyDocument.click('create-buy-group')
  }

  askToJoin() {
    this.spyDocument.click('request-button')
  }

  chooseAnotherBuyGroup() {
    this.page.selectBuyGroup({ id: this.SELECTED_BUY_GROUP })
  }

  createRequest(requiredInformation) {
    this.clickRequestToJoin()
    this.fillRequirements(requiredInformation)
    this.askToJoin()
  }

  createParticipation(information) {
    this.createRequest(information)
    this.acceptAsParticipant()
    this.showParticipations()
  }

  acceptAsParticipant() {
    this.spyDocument.click('accept-as-participant-button0')
  }

  showParticipations() {
    return this.spyDocument.click('participations-tab')
  }

  rejectParticipant() {
    this.spyDocument.click('reject-involved-button-0')
  }

  hasAParticipation() {
    return this.spyDocument.text().includes('participations-0')
  }

  hasRequestToJoinDisabled() {
    return this.spyDocument.getTemplateById('request-to-join-button').includes('disabled')
  }

  showsRequestModal() {
    return this.spyDocument.text().includes('request-button')
  }

  showsCreationModal() {
    return this.spyDocument.text().includes('modal is-active')
  }

  showsAnotherDetail() {
    return this.spyDocument.text().includes(this.DETAIL_ID + this.SELECTED_BUY_GROUP)
  }

  showsDetail() {
    const anyBuyGroupId = '5'

    return this.spyDocument.text().includes(this.DETAIL_ID + anyBuyGroupId)
  }

  showsList() {
    return this.includes('buyGroup1')
  }

  showsInList(buyGroup) {
    const noFinalizationDate = '--/--/--'
    const noParticipants = '<i class="fas fa-users"></i>0'

    return this.includes('buyGroup6') && this.includes(buyGroup.title) && this.includes(noFinalizationDate) && this.includes(noParticipants)
  }

  showsInDetail(buyGroup) {
    return this.includes('buyGroupDetail6') && this.showsInList(buyGroup) && this.includes(buyGroup.description) && this.includes(buyGroup.requirements)
  }

  includes(text) {
    return this.spyDocument.text().includes(text)
  }

  movedToLogin() {
    return this.user.movedToLogin()
  }
}
