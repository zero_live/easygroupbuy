
export class SpyDocument {
  constructor() {
    this.elements = {}

    this.overrideBrowserDocument()
  }

  fill(id, value) {
    const element = this.getElementById(id)

    element.value = value.toString()
  }

  input(id, value) {
    const element = this.getElementById(id)

    element.input(value.toString())
  }

  click(id) {
    const element = this.getElementById(id)

    element.click()
  }

  getClassesById(id) {
    const element = this.getElementById(id)

    return element.className
  }

  getElementById(id) {
    if (!this.elements[id]) {
      this.elements[id] = new Element(id, { clearAllFocused: this.clearAllFocused.bind(this) })
    }

    return this.elements[id]
  }

  text() {
    let text = ''

    const ids = Object.keys(this.elements)
    ids.forEach((id) => {
      text += this.elements[id].innerHTML
    })

    return text
  }

  getTemplateById(id) {
    const elementTemplate = this.elementTemplates().find((template) => {
      return template.includes(id)
    })

    return elementTemplate
  }

  //private

  clearAllFocused() {
    const ids = Object.keys(this.elements)
    ids.forEach((id) => {
      this.elements[id].clearFocus()
    })
  }

  overrideBrowserDocument() {
    global.document = this
  }

  elementTemplates() {
    const ELEMENT_SEPARATOR = '<'

    const templates = this.text().split(ELEMENT_SEPARATOR)

    return templates
  }
}

class Element {
  constructor(id, documentActions) {
    this.innerHTML = ''
    this.value
    this.className
    this.eventListeners = {}
    this.id = id
    this._isFocus = false
    this.clearAllFocused = documentActions.clearAllFocused
  }

  addEventListener(event, callback) {
    this.eventListeners[event] = callback
  }

  click() {
    this.eventListeners.click({ target: { id: this.id } })
  }

  input(value) {
    this.eventListeners.input({ target: { value: value }})
  }

  focus() {
    this.clearAllFocused()

    this._isFocus = true
  }

  clearFocus() {
    this._isFocus = false
  }

  isFocus() {
    return this._isFocus
  }
}
