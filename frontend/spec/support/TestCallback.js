
export class TestCallback {
  constructor({ andReturn } = {}) {
    this.valueToReturn = andReturn
    this.data
    this.isCalled = false

    this.execute = this.execute.bind(this)
  }

  execute(data) {
    this.isCalled = true
    this.data = data

    return this.valueToReturn
  }

  hasBeenCalled() {
    return this.isCalled
  }

  hasBeenCalledWith() {
    return this.data
  }
}
