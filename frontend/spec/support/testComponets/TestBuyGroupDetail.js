import { BuyGroupDetail } from '../../../public/js/components/buyGroupDetail/BuyGroupDetail'
import { toLocaleDate } from '../../../public/js/toLocaleDate'
import { SpyDocument } from '../SpyDocument'

export class TestBuyGroupDetail {
  constructor(data) {
    this.spyDocument = new SpyDocument()
    this.detail = new BuyGroupDetail(data)
  }

  draw() {
    this.detail.draw()

    return this
  }

  requestToJoin() {
    this.spyDocument.click('request-to-join-button')
  }

  showParticipations() {
    this.spyDocument.click('participations-tab')
  }

  showRequests() {
    this.spyDocument.click('requests-tab')
  }

  acceptRequest() {
    this.spyDocument.click('accept-as-participant-button0')
  }

  showsDetail(buyGroup) {
    const finalizationDate = toLocaleDate(buyGroup.finalizationDate)

    return this.spyDocument.text().includes('Descripción:') &&
    this.spyDocument.text().includes('Información necesaria para participar:') &&
    this.spyDocument.text().includes(buyGroup.title) &&
    this.spyDocument.text().includes(finalizationDate) &&
    this.spyDocument.text().includes(buyGroup.currentParticipants) &&
    this.spyDocument.text().includes(buyGroup.maximumParticipants) &&
    this.spyDocument.text().includes(buyGroup.status) &&
    this.spyDocument.text().includes(buyGroup.description) &&
    this.spyDocument.text().includes(buyGroup.requirements)
  }

  firstRequest() {
    return this.spyDocument.getTemplateById('requests-0')
  }

  secondRequest() {
    return this.spyDocument.getTemplateById('requests-1')
  }

  firstParticipation() {
    return this.spyDocument.getTemplateById('participations-0')
  }

  secondParticipation() {
    return this.spyDocument.getTemplateById('participations-1')
  }

  isActiveParticipationsTab() {
    const template = this.spyDocument.getTemplateById('participations-li')

    return template.includes('is-active')
  }

  isActiveRequestsTab() {
    const template = this.spyDocument.getTemplateById('requests-li')

    return template.includes('is-active')
  }

  showsInvolvedTabs() {
    return this.spyDocument.text().includes('tab-list')
  }

  canAcceptAParticipation() {
    return this.spyDocument.text().includes('Aceptar')
  }
}
