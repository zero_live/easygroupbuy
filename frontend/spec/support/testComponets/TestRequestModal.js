import { RequestModal } from '../../../public/js/components/RequestModal'
import { SpyDocument } from '../SpyDocument'

export class TestRequestModal {
  constructor(data) {
    this.spyDocument = new SpyDocument()
    this.modal = new RequestModal(data)
    this.modal.draw()
  }

  show() {
    this.modal.show()

    return this
  }

  close() {
    this.spyDocument.click('close-request-modal')
  }

  cancel() {
    this.spyDocument.click('cancel-request-modal')
  }

  fill(text) {
    this.spyDocument.input('requirements', text)
  }

  requestJoin() {
    this.spyDocument.click('request-button')
  }


  text() {
    return this.spyDocument.text()
  }

  isHidden() {
    return (this.spyDocument.text() === '')
  }

  isRequirementsFocused() {
    const element = this.spyDocument.getElementById('requirements')

    return element.isFocus()
  }

  hasRequestButtonDisabled() {
    const template = this.spyDocument.getTemplateById('request-button')

    return template.includes('disabled')
  }
}
