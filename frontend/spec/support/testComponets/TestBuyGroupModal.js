import { BuyGroupModal } from '../../../public/js/components/BuyGroupModal'
import { SpyDocument } from '../SpyDocument'

export class TestBuyGroupModal {
  constructor(data = {}) {
    this.spyDocument = new SpyDocument()
    this.modal = new BuyGroupModal(data)
  }

  hide() {
    this.modal.hide()
  }

  show() {
    this.modal.show()
  }

  fill(buyGroup) {
    this.spyDocument.fill('new-title', buyGroup.title)
    this.spyDocument.fill('new-maximum-participants', buyGroup.maximumParticipants)
    this.spyDocument.fill('new-finalization-date', buyGroup.finalizationDate)
    this.spyDocument.fill('new-description', buyGroup.description)
    this.spyDocument.fill('new-requirements', buyGroup.requirements)
  }

  create() {
    this.modal.create()
  }

  showsErrors() {
    return (this.spyDocument.text().includes('Informatión requerida'))
  }

  isHidden() {
    this.modal.draw()

    return (this.spyDocument.text() === 'Terminado!')
  }

  isTitleFocused() {
    const element = this.spyDocument.getElementById('new-title')

    return element.isFocus()
  }
}
