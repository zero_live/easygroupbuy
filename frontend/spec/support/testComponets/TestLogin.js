import { Loging } from "../../../public/js/components/navbar/Login"

export class TestLogin {
  constructor(data) {
    this.data = data
    this.component = new Loging(data)
  }

  hasButton() {
    const template = this.component.template()
    const userIcon = 'fas fa-user'
    const loginText = '<span>Login</span>'
    const loginUrl = 'fake_login.html'

    return template.includes(userIcon) &&
    template.includes(loginText) &&
    template.includes(loginUrl)
  }

  hasAvatar() {
    const template = this.component.template()
    const userImage = this.data.user.image()
    const userName = this.data.user.name()

    return template.includes(userImage) &&
    template.includes(userName)
  }
}
