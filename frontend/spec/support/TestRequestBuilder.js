
export class TestRequestBuilder {
  static default() {
    const request = new TestRequestBuilder()

    return request.build()
  }

  constructor() {
    this.request = {
      buyGroupId: 'buyGroupId',
      id: 0,
      information: 'someRequirements',
      type: 'request'
    }
  }

  withOneComment() {
    this.request.comments = [ { comment: 'anyComment', id: 1 } ]

    return this
  }

  withTwoComments() {
    this.request.comments = [
      { comment: 'anyComment', id: 1 },
      { comment: 'anotherComment', id: 2 }
    ]

    return this
  }

  build() {
    return this.request
  }
}
