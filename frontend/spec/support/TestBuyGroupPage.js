const REQUIRED_INFORMATION_FILLED = 'Some Required Information for fill'

export class BuyGroupPage {
  constructor() {
    cy.visit('/')
  }

  selectLastBuyGroup() {
    cy.get('#buyGroup5').click()

    return true
  }

  clickOnNewBuyGroup() {
    cy.get('#new-buy-group-button').click()
  }

  fillNewBuyGroup() {
    cy.get('#new-title').type('new-title')
    cy.get('#new-description').type('new-description')
    cy.get('#new-requirements').type('new-requirements')
  }

  confirmCreation() {
    cy.get('#create-buy-group').click()
  }

  requestToJoin() {
    cy.get('#request-to-join-button').click()
  }

  fillRequirements() {
    cy.get('#requirements').type(REQUIRED_INFORMATION_FILLED)
  }

  request() {
    cy.get('#request-button').click()
  }

  createRequest() {
    this.requestToJoin()
    this.fillRequirements()
    this.request()
  }

  acceptRequest() {
    cy.get('#accept-as-participant-button0').click()
  }

  showParticipationList() {
    cy.get('#participations-tab').click()
  }

  isShowingParticipations() {
    cy.get('#participations-0')
    cy.contains('Zero Live')
    cy.contains('@zerolive')
    cy.contains(REQUIRED_INFORMATION_FILLED)

    return true
  }

  isShowingRequester() {
    cy.get('#accept-as-participant-button0')
    cy.contains('Zero Live')
    cy.contains('@zerolive')
    cy.contains(REQUIRED_INFORMATION_FILLED)

    return true
  }

  showsCreationFeedback() {
    cy.contains('Terminado!')

    return true
  }

  showsList() {
    cy.contains("Lluis' Group 1/10 1/1/2222 OPEN")
    cy.contains("Mario's Group 2/10 2/2/2222 OPEN")
    cy.contains("Jaume's Group 3/10 3/3/2222 OPEN")
    cy.contains("Vilva's Group 4/10 4/4/2222 OPEN")
    cy.contains("Zero's Group 5/10 5/5/2222 OPEN")

    return true
  }

  showsLastDetail() {
    cy.get('#buyGroupDetail5')

    return true
  }

  showsNewBuyGroupInList() {
    cy.contains("new-title 0 --/--/-- OPEN")

    return true
  }

  showsNewBuyGroupDetail() {
    cy.contains('new-description')
    cy.contains('new-requirements')

    return true
  }

  includes(text) {
    cy.contains(text)

    return true
  }
}
