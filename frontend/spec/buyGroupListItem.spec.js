import { buyGroupListItem } from '../public/js/buyGroupListItem'
import { BuyGroupSamples } from './support/BuyGroupSamples'
import { toLocaleDate } from '../public/js/toLocaleDate'

describe('buyGroupListItem', () => {
  let buyGroup

  beforeEach(() => {
    buyGroup = BuyGroupSamples.some()
  })

  it('shows BuyGroup summary data', () => {

    const item = new TestBuyGroupListItem(buyGroup)

    expect(item.includes(buyGroup)).toEqual(true)
    expect(item.isHighligthed()).toEqual(false)
  })

  it('can be highlithed', () => {
    buyGroup.isActive = true

    const item = new TestBuyGroupListItem(buyGroup)

    expect(item.isHighligthed()).toEqual(true)
  })
})

class TestBuyGroupListItem {
  constructor(data) {
    this.component = buyGroupListItem(data)
  }

  includes(buyGroup) {
    const finalizationDate = toLocaleDate(buyGroup.finalizationDate)
    const hasTitle = this.component.includes(buyGroup.title)
    const hasCurrentParticipants = this.component.includes(buyGroup.currentParticipants)
    const hasMaximumParticipants = this.component.includes(buyGroup.maximumParticipants)
    const hasFinalizationDate = this.component.includes(finalizationDate)
    const hasStatus = this.component.includes(buyGroup.status)

    return hasTitle && hasCurrentParticipants && hasMaximumParticipants && hasFinalizationDate && hasStatus
  }

  isHighligthed() {
    return this.component.includes('has-background-grey-lighter')
  }
}
