import { BuyGroupSamples } from './support/BuyGroupSamples'
import { SpyDocument } from './support/SpyDocument'
import { BuyGroupList } from '../public/js/BuyGroupList'

describe('BuyGroupList', () => {
  it('draws buy groups', () => {
    const buyGroup = BuyGroupSamples.some()

    const list = new TestBuyGroupList({ buyGroups: [buyGroup], selected: buyGroup }).draw()

    expect(list.text()).toContain(buyGroup.title)
    expect(list.countHighLithsItems()).toEqual(1)
  })

  it('shows the newest BuyGroup in top (first)', () => {
    const buyGroups = BuyGroupSamples.many()
    const newestBuyGroup = lastAddedBuyGroup()

    const list = new TestBuyGroupList({ buyGroups, selected: buyGroups[0] }).draw()

    expect(list.isFirstListElement(newestBuyGroup.id)).toEqual(true)
  })

  function lastAddedBuyGroup() {
    const buyGroups = BuyGroupSamples.many()

    return buyGroups[buyGroups.length - 1]
  }
})

class TestBuyGroupList {
  constructor(data) {

    this.spyDocument = new SpyDocument()
    this.list = new BuyGroupList(data)
  }

  draw() {
    this.list.draw()

    return this
  }

  text() {
    return this.spyDocument.text()
  }

  countHighLithsItems() {
    const IS_ACTIVE = 'has-background-grey-lighter'
    const elements = this.text().split(IS_ACTIVE)
    const totalElements = elements.length
    const baseElement = 1

    return totalElements - baseElement
  }

  isFirstListElement(id) {
    const PARTIAL_ID = 'buyGroup'
    const FIRST_ELEMENT_POSITION = 2
    const elements = this.text().split('id=')
    const firstElement = elements[FIRST_ELEMENT_POSITION]

    return firstElement.includes(PARTIAL_ID + id)
  }
}
