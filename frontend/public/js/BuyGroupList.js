import { buyGroupListItem } from './buyGroupListItem.js'

const NEW_BUTTON_ID = 'new-buy-group-button'

export class BuyGroupList {
  constructor(data) {
    this.document = document
    this.element = this.document.getElementById('buy-group-list')

    this.decorators = []
    this.selected = data.selected || {}
    this.data = data
  }

  draw() {
    const decorators = this.decorate(this.data.buyGroups)

    this.element.innerHTML = this.toHtml(decorators)
    this.addOnClicks(this.data.selectBuyGroup)
  }

  //private

  addOnClicks(selectBuyGroup) {
    this.decorators.forEach((decorated) => {
      const callback = () => { selectBuyGroup(decorated) }

      this.addOnClickEvent(decorated.htmlId, callback)
    })

    this.addOnClickEvent(NEW_BUTTON_ID, this.showCreateBuyGroup.bind(this))
  }

  decorate(buyGroups) {
    const decorators = buyGroups.map((buyGroup) => {
      const decorated = this.decorateOne(buyGroup)
      this.decorators.push(decorated)

      return buyGroupListItem(decorated)
    })

    return decorators
  }

  decorateOne(buyGroup) {
    const decorated = buyGroup

    decorated.isActive = this.isActive(buyGroup)
    decorated.htmlId = 'buyGroup' + buyGroup.id

    return decorated
  }

  toHtml(decorators) {
    const newestUp = this.order(decorators)

    return this.addButtonHtml() + newestUp.join('')
  }

  addButtonHtml() {
    return `
      <button class="button is-info is-primary" id="${NEW_BUTTON_ID}">
        <i class="fas fa-plus"></i>
      </button>
    `
  }

  order(items) {
    return items.reverse()
  }

  isActive(buyGroup) {
    return (buyGroup.id === this.selected.id)
  }

  addOnClickEvent(id, callback) {
    this.addEvent('click', id, callback)
  }

  addEvent(event, id, callback) {
    const element = this.document.getElementById(id)
    element.addEventListener(event, callback)
  }

  showCreateBuyGroup() {
    this.data.newBuyGroup()
  }
}
