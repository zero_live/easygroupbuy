
const NO_DATE_SYMBOL = '--/--/--'

export const toLocaleDate = (stringDate) => {
  if (isEmpty(stringDate)) { return NO_DATE_SYMBOL }

  const date = new Date(stringDate)
  return date.toLocaleDateString()
}

const isEmpty = (string) => {
  return (string === '')
}
