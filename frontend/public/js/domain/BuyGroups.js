import { BuyGroup } from "./BuyGroup.js"

export class BuyGroups {
  constructor(rawBuyGroups) {
    this.buyGroups = this.buildBuyGroups(rawBuyGroups)
  }

  toDictionaries() {
    return this.buyGroups.map(buyGroup => buyGroup.toDictionary())
  }

  //private

  buildBuyGroups(rawBuyGroups) {
    return rawBuyGroups.map(rawBuyGroup => new BuyGroup(rawBuyGroup))
  }
}
