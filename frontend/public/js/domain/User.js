
export class User {
  static isLogged() {
    return !!localStorage.getItem('userToken')
  }

  static image() {
    return localStorage.getItem('userPhotoUrl')
  }

  static name() {
    return localStorage.getItem('userUsername')
  }

  static loginUrl() {
    let url = 'login.html'

    if (this.isDevelopmentUrl()) { url = 'fake_login.html' }

    return url
  }

  static moveToLogin() {
    window.location.href = this.loginUrl()
  }

  static moveToLoginIfIsUnidentified() {
    if (!this.isLogged()) {
      window.location.href = this.loginUrl()
    }
  }

  //private

  static isDevelopmentUrl() {
    const location = window.location.href

    return location.includes('localhost') || location.includes('frontend')
  }
}
