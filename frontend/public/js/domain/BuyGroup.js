
export class BuyGroup {
  static fromForm(data, id) {
    const initialCurrentParticipants = 0
    const defaultStatus = 'OPEN'
    const completedData = {
      id: id,
      title: data.title,
      currentParticipants: initialCurrentParticipants,
      maximumParticipants: data.maximumParticipants,
      finalizationDate: data.finalizationDate,
      status: defaultStatus,
      description: data.description,
      requirements: data.requirements,
    }

    return new BuyGroup(completedData)
  }

  constructor(data) {
    this.data = data
  }

  increaseCurrentParticipantsByOne() {
    this.data.currentParticipants += 1

    this.updateStatus()
  }

  decreaseCurrentParticipantsByOne() {
    this.data.currentParticipants -= 1

    this.updateStatus()
  }

  toDictionary() {
    return this.data
  }

  //private

  updateStatus() {
    if (this.isReachedMaximumParticipants()) {
      this.data.status = 'CLOSED'
    }
    if (!this.isReachedMaximumParticipants()) {
      this.data.status = 'OPEN'
    }
  }

  isReachedMaximumParticipants() {
    return (this.data.currentParticipants >= this.data.maximumParticipants)
  }
}
