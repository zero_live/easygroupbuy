import { BuyGroups } from "./BuyGroups.js"
import { BuyGroup } from "./BuyGroup.js"

const PARTICIPATION_TYPE = 'participation'
const REQUEST_TYPE = 'request'

export class Actions {
  constructor(collection) {
    this.collection = collection

    this.createBuyGroups = this.createBuyGroups.bind(this)
    this.retrieveRequests = this.retrieveRequests.bind(this)
    this.retrieveBuyGroups = this.retrieveBuyGroups.bind(this)
    this.retrieveParticipations = this.retrieveParticipations.bind(this)
    this.becomeParticipation = this.becomeParticipation.bind(this)
    this.requestToJoin = this.requestToJoin.bind(this)
    this.updateBuyGroup = this.updateBuyGroup.bind(this)
    this.comment = this.comment.bind(this)
    this.rejectInvolved = this.rejectInvolved.bind(this)
  }

  createBuyGroups(data) {
    const buyGroup = this.newBuyGroup(data)

    this.collection.store(buyGroup, 'buyGroups')

    return buyGroup
  }

  retrieveBuyGroups() {
    const rawBuyGroups = this.collection.retrieveAll('buyGroups')
    const buyGroups = new BuyGroups(rawBuyGroups)

    return buyGroups.toDictionaries()
  }

  requestToJoin(buyGroupId, requestFromForm) {
    const request = this.newRequest(buyGroupId, requestFromForm)

    this.collection.store(request, buyGroupId + '-involved')

    return request
  }

  retrieveRequests(buyGroupId) {
    const involveds = this.collection.retrieveAll(buyGroupId + '-involved')
    const requests = involveds.filter(involved => involved.type === REQUEST_TYPE)

    return requests
  }

  becomeParticipation(request) {
    const participation = { ...request, ...{ type: PARTICIPATION_TYPE } }
    this.updateInvolved(participation)

    this.increaseCurrentParticipantsByOne(request.buyGroupId)

    return participation
  }

  rejectInvolved(involved) {
    const rejected = { ...involved, ...{ isRejected: true } }
    this.updateInvolved(rejected)

    if (this.isParticipation(rejected)) {
      this.decreaseCurrentParticipantsByOne(rejected.buyGroupId)
    }

    return rejected
  }

  retrieveParticipations(buyGroupId) {
    const involveds = this.collection.retrieveAll(buyGroupId + '-involved')
    const requests = involveds.filter(involved => involved.type === PARTICIPATION_TYPE)

    return requests
  }

  updateBuyGroup(rawBuyGroup) {
    const buyGroup = new BuyGroup(rawBuyGroup).toDictionary()

    this.collection.update(buyGroup, 'buyGroups')

    return buyGroup
  }

  comment(involved, comment) {
    const commented = this.addComment(involved, comment)

    this.updateInvolved(commented)

    return commented
  }

  //private

  increaseCurrentParticipantsByOne(id) {
    const buyGroup = this.retrieveBuyGroup(id)

    buyGroup.increaseCurrentParticipantsByOne()

    this.updateBuyGroup(buyGroup)
  }

  decreaseCurrentParticipantsByOne(id) {
    const buyGroup = this.retrieveBuyGroup(id)

    buyGroup.decreaseCurrentParticipantsByOne()

    this.updateBuyGroup(buyGroup)
  }

  retrieveBuyGroup(id) {
    const rawBuyGroup = this.collection.retrieve(id, 'buyGroups')

    return new BuyGroup(rawBuyGroup)
  }

  updateInvolved(involved) {
    const collection = involved.buyGroupId + '-involved'

    this.collection.update(involved, collection)
  }

  addComment(involved, comment) {
    const copiedInvolved = this.retrieveInvolved(involved.id, involved.buyGroupId)

    if (!copiedInvolved.comments) { copiedInvolved.comments = [] }
    copiedInvolved.comments.push({ ...comment, ...this.newCommentId(copiedInvolved) })

    return copiedInvolved
  }

  retrieveInvolved(id, buyGroupId) {
    const involveds = this.collection.retrieveAll(buyGroupId + '-involved')

    return involveds.find(involved => involved.id === id)
  }

  newRequest(buyGroupId, requestFromForm) {
    return {
      buyGroupId: buyGroupId,
      type: REQUEST_TYPE,
      id: this.nextId(buyGroupId),
      information: requestFromForm.information
    }
  }

  newBuyGroup(data) {
    const newId = this.nextId('buyGroups')

    const buyGroup = BuyGroup.fromForm(data, newId)
    return buyGroup.toDictionary()
  }

  nextId(collection) {
    return this.collection.size(collection)
  }

  newCommentId(involved) {
    const comments = involved.comments

    return { id: comments.length + 1 }
  }

  isParticipation(involved) {
    return (involved.type === PARTICIPATION_TYPE)
  }
}
