
export class Collection {
  constructor(initialValues) {
    this.values = {}
    this.values['buyGroups'] = initialValues || []
  }

  retrieveAll(collection) {
    if (!this.exists(collection)) { this.create(collection) }

    return [...this.values[collection]]
  }

  retrieve(id, collection) {
    const value = this.values[collection].find((value) => {
      return (value.id === id)
    })

    return value
  }

  store(value, collection) {
    if (!this.exists(collection)) { this.create(collection) }
    this.values[collection].push(value)
  }

  update(item, collection) {
    this.values[collection] = this.values[collection].map((value) => {
      if (value.id === item.id) {
        return item
      }

      return value
    })
  }

  remove(id, collection) {
    this.values[collection] = this.values[collection].filter(value => value.id !== id)
  }

  size(collection) {
    if (!this.exists(collection)) { return 0 }

    return this.values[collection].length + 1
  }

  //private

  exists(collection) {
    return !!this.values[collection]
  }

  create(collection) {
    this.values[collection] = []
  }
}
