import { CreatedBuyGroupSnackbar } from "../CreatedBuyGroupSnackbar.js"
import { translate } from "../translate.js"
import { Component } from "./Component.js"

const TITLE_ID = 'new-title'
const FINALIZATION_DATE_ID = 'new-finalization-date'
const MAXIMUM_PARTICIPANTS_ID = 'new-maximum-participants'
const DESCRIPTION_ID = 'new-description'
const REQUIREMENTS_ID = 'new-requirements'
const MODAL_ID = 'buy-group-modal'

const IDS = {
  title: TITLE_ID,
  finalizationDate: FINALIZATION_DATE_ID,
  maximumParticipants: MAXIMUM_PARTICIPANTS_ID,
  description: DESCRIPTION_ID,
  requirements: REQUIREMENTS_ID
}

export class BuyGroupModal extends Component {
  constructor(data={}) {
    super(MODAL_ID)
    this.show = this.show.bind(this)

    this.snackbar = new CreatedBuyGroupSnackbar(this.document)
    this.createBuyGroup = data.create
    this.reloadPage = data.reload
    this.selectBuyGroup = data.selectBuyGroup
    this.hasToBeHidden = true
    this.user = data.user
    this._buyGroup = data.buyGroup || this.emptyBuyGroup()
  }

  hide() {
    this.hasToBeHidden = true

    this.draw()
  }

  show() {
    if (this.user.isLogged()) {
      this.hasToBeHidden = false

      this.draw()
      this.focusTitle()
    } else {
      this.user.moveToLogin()
    }
  }

  create() {
    this._buyGroup = this.newBuyGroup()
    if (!this.hasValidInformation()) { return this.showWarnings() }

    const createdBuyGroup = this.createBuyGroup(this._buyGroup)
    this.snackbar.show()
    this.hide()
    this.selectBuyGroup(createdBuyGroup)
    this.reloadPage()
  }

  //private

  showWarnings() {
    this.draw()

    const fields = Object.keys(this._buyGroup)
    fields.forEach((field) => {
      const element = this.getElementById(IDS[field])

      element.value = this._buyGroup[field]
    })
  }

  addCallbacks() {
    this.addOnClick('close-modal', this.hide.bind(this))
    this.addOnClick('cancel-modal', this.hide.bind(this))
    this.addOnClick('create-buy-group', this.create.bind(this))
  }

  newBuyGroup() {
    return { ...this._buyGroup, ...this.fieldsValues() }
  }

  hasValidInformation() {
    return (this.validTitle() && this.validDescription() && this.validRequirements())
  }

  title() {
    return this.getValue(TITLE_ID)
  }

  description() {
    return this.getValue(DESCRIPTION_ID)
  }

  requirements() {
    return this.getValue(REQUIREMENTS_ID)
  }

  validTitle() {
    return (this._buyGroup.title !== '')
  }

  validRequirements() {
    return (this._buyGroup.requirements !== '')
  }

  validDescription() {
    return (this._buyGroup.description !== '')
  }

  titleWarning() {
    return this.warning(this.validTitle())
  }

  descriptionWarning() {
    return this.warning(this.validDescription())
  }

  requirementsWarning() {
    return this.warning(this.validRequirements())
  }

  warning(isValid) {
    let text = ' ' + translate('missingInfo')

    if (isValid) { text = '' }

    return `<span class='has-text-danger has-text-weight-bold'>*${text}</span>`
  }

  emptyBuyGroup() {
    return {
      title: '',
      finalizationDate: '',
      maximumParticipants: '',
      description: '',
      requirements:''
    }
  }

  fieldsValues() {
    const title = this.title()
    const rawMaximumParticipants = this.getValue(MAXIMUM_PARTICIPANTS_ID)
    const maximumParticipants = parseInt(rawMaximumParticipants)
    const finalizationDate = this.getValue(FINALIZATION_DATE_ID)
    const description = this.description()
    const requirements = this.requirements()

    return { title, maximumParticipants, finalizationDate, description, requirements }
  }

  template() {
    if (this.hasToBeHidden) { return '' }

    return `
      <div class="modal is-active">
        <div class="modal-background"></div>
        <div class="modal-card">
          <header class="modal-card-head">
            <p class="modal-card-title">${translate('buyGroup')}</p>
            <button class="delete" aria-label="close" id="close-modal"></button>
          </header>
          <section class="modal-card-body">

            <div class="field">
              <label class="label">${translate('title')} ${this.titleWarning()}</label>
              <div class="control">
                <input id="${TITLE_ID}" class="input" type="text" value="${this._buyGroup.title}">
              </div>
            </div>

            <div class="field">
              <label class="label">${translate('maximumParticipants')}</label>
              <div class="control">
                <input id="${MAXIMUM_PARTICIPANTS_ID}" class="input" type="number" value="${this._buyGroup.maximumParticipants}">
              </div>
            </div>

            <div class="field">
              <label class="label">${translate('finalizationDateToJoin')}</label>
              <div class="control">
                <input id="${FINALIZATION_DATE_ID}" class="input" type="date" value="${this._buyGroup.finalizationDate}">
              </div>
            </div>

            <div class="field">
              <label class="label">${translate('description')} ${this.descriptionWarning()}</label>
              <div class="control">
                <textarea id="${DESCRIPTION_ID}" class="textarea">
                  ${this._buyGroup.description}
                </textarea>
              </div>
            </div>

            <div class="field">
              <label class="label">${translate('requiredInfo')} ${this.requirementsWarning()}</label>
              <div class="control">
                <textarea id="${REQUIREMENTS_ID}" class="textarea">
                  ${this._buyGroup.requirements}
                </textarea>
              </div>
            </div>

          </section>
          <footer class="modal-card-foot">
            <button class="button is-success" id="create-buy-group">${translate('done')}</button>
            <button class="button" id="cancel-modal">${translate('cancel')}</button>
          </footer>
        </div>
      </div>
    `
  }

  getValue(id) {
    const element = this.getElementById(id)

    return element.value
  }

  focusTitle() {
    this.getElementById(TITLE_ID).focus()
  }
}
