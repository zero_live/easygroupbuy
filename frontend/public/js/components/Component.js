
export class Component {
  constructor(id) {
    this.document = document
    this.element = this.getElementById(id)

    this.data = {}
  }

  draw() {
    this.element.innerHTML = this.template()

    this.addCallbacks()
  }

  //private

  template() {}

  addCallbacks() {}

  getElementById(id) {
    return this.document.getElementById(id)
  }

  addOnClick(id, callback) {
    this.addEventListener('click', id, callback)
  }

  addEventListener(event, id, callback) {
    const element = this.getElementById(id)
    if (element) {
      element.addEventListener(event, callback)
    }
  }
}
