import { participantsSummary } from '../../participantsSummary.js'
import { toLocaleDate } from '../../toLocaleDate.js'
import { translate } from '../../translate.js'
import { Component } from '../Component.js'
import { Involved } from './Involved.js'

const JOIN_ID = 'request-to-join-button'
const PARTICIPATIONS_TAB_ID = 'participations-tab'
const REQUESTS_TAB_ID = 'requests-tab'
const NO_TABS = ''
const PARTICIPATIONS_GROUP = 'participations'
const REQUESTERS_GROUP = 'requests'
const ACCEPT_ID = 'accept-as-participant-button'
const EDIT_ID = 'edit-buy-group-button'

export class BuyGroupDetail extends Component {
  constructor(data) {
    super('buy-group-detail')

    this.data = data
    this.currentTab = REQUESTERS_GROUP
    this.collapsed = []
  }

  //private

  template() {
    const buyGroup = this.data.buyGroup
    const finalizationDate = toLocaleDate(buyGroup.finalizationDate)

    return `
      <div class="box" id="buyGroupDetail${buyGroup.id}">
        <div class="level">
          <div class="level-item">${finalizationDate}</div>
          <div class="level-item">
            ${participantsSummary(buyGroup)}
          </div>
          <div class="level-item">${buyGroup.status}</div>
        </div>
        <div class=="level">
          <div class="level-right">
            ${this.requestToJoinButton()}
          </div>
        </div>
        <div class="level">
          <div class="level-item is-size-1">
            ${buyGroup.title} <i class="fas fa-pen is-size-7 has-text-link custom-as-link" id="${EDIT_ID}"></i>
          </div>
        </div>
        <div class="level">
          <div class="level-left is-size-3">
            ${translate('description')}:
          </div>
        </div>
        <p class="has-text-justified">
          ${buyGroup.description}
        </p>
        <div class="level">
          <div class="level-left is-size-3">
            ${translate('requiredInfo')}:
          </div>
        </div>
        <p class="has-text-justified">
          ${buyGroup.requirements}
        </p>

      </div>
      ${this.tabs()}
    `
  }

  requestToJoinButton() {
    let disabled = 'disabled'
    if (this.hasNoInvolved()) { disabled = '' }

    return `<button id="${JOIN_ID}" class="button is-info" ${disabled}>${translate('join')}</button>`
  }

  tabs() {
    if (this.hasNoInvolved()) { return NO_TABS }

    return `
      <div class="tabs is-centered" id="tab-list">
        <ul>
          <li class="${this.isActive(REQUESTERS_GROUP)}" id="requests-li">
            <a id="${REQUESTS_TAB_ID}">
              <span class="icon is-small"><i class="fas fa-sign-in-alt" aria-hidden="true"></i></span>
              <span>${translate('interested')}</span>
            </a>
          </li>
          <li class="${this.isActive(PARTICIPATIONS_GROUP)}" id="participations-li">
            <a id="${PARTICIPATIONS_TAB_ID}">
              <span class="icon is-small"><i class="fas fa-users" aria-hidden="true"></i></span>
              <span>${translate('participants')}</span>
            </a>
          </li>
        </ul>
      </div>
      ${this.involved(REQUESTERS_GROUP)}
      ${this.involved(PARTICIPATIONS_GROUP)}
    `
  }

  showParticipations() {
    this.currentTab = PARTICIPATIONS_GROUP

    this.draw()
  }

  showRequesters() {
    this.currentTab = REQUESTERS_GROUP

    this.draw()
  }

  involved(groupName) {
    const hasToShow = (!this.notSelected(groupName))

    return new Involved(this[groupName].bind(this), groupName, this.collapsed).template({ hasToShow })
  }

  toogleInvolved(involvedId) {
    if (this.collapsed.includes(involvedId)) {
      this.collapsed = this.collapsed.filter(id => id !== involvedId)
    } else {
      this.collapsed.push(involvedId)
    }

    this.draw()
  }

  isActive(group) {
    if (this.notSelected(group)) { return '' }

    return 'is-active'
  }

  hasNoInvolved() {
    return (!this.hasRequests() && !this.hasParticipations())
  }

  hasParticipations() {
    return this.hasInvolved(PARTICIPATIONS_GROUP)
  }

  hasRequests() {
    return this.hasInvolved(REQUESTERS_GROUP)
  }

  hasInvolved(group) {
    return (this[group]().length !== 0)
  }

  acceptAsParticipant(event) {
    const requestId = 1
    const id = event.target.id.split(ACCEPT_ID)[requestId]

    const toParticipant = this.requests().find((request) => {
      return (request.id === parseInt(id))
    })

    this.data.becomeParticipation(toParticipant)
    this.data.reload()
  }

  participations() {
    return this.data.retrieveParticipations(this.data.buyGroup.id)
  }

  requests() {
    return this.data.retrieveRequests(this.data.buyGroup.id)
  }

  addCallbacks() {
    this.addOnClick(JOIN_ID, this.data.requestToJoin)
    this.addOnClick(PARTICIPATIONS_TAB_ID, this.showParticipations.bind(this))
    this.addOnClick(REQUESTS_TAB_ID, this.showRequesters.bind(this))
    this.addOnClick(EDIT_ID, this.data.edit)
    this.addCommentCallbacks()
    this.addCollapseInvolvedCallbacks()
    this.addAcceptCallbacks()
    this.addRejectParticipantCallbaks()
  }

  comment(involved) {
    const commentInput = this.getElementById(involved.id + '-comment-input')
    const comment = commentInput.value

    this.data.comment(involved, { comment: comment })
    this.draw()
  }

  requestToJoin() {
    if (!this.hasNoInvolved()) { return }

    this.data.requestToJoin()
  }

  addAcceptCallbacks() {
    this.requests().forEach((request) => {
      this.addOnClick(ACCEPT_ID + request.id, this.acceptAsParticipant.bind(this))
    })
  }

  addCollapseInvolvedCallbacks() {
    this.involveds().forEach((involved) => {
      const id = involved.id + '-collapse'

      this.addOnClick(id, this.toogleInvolved.bind(this, involved.id))
    })
  }

  addCommentCallbacks() {
    this.involveds().forEach((involved) => {
      const id = involved.id + '-comment-button'

      this.addOnClick(id, this.comment.bind(this, involved))
    })
  }

  addRejectParticipantCallbaks() {
    this.involveds().forEach((involved) => {
      const id = 'reject-involved-button-' + involved.id

      this.addOnClick(id, this.rejectParticipant.bind(this, involved))
    })
  }

  rejectParticipant(involved) {
    this.data.rejectParticipant(involved)

    this.data.reload()
  }

  involveds() {
    return [...this.requests(), ...this.participations()]
  }

  notSelected(groupName) {
    return (this.currentTab !== groupName)
  }
}
