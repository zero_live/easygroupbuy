import { translate } from '../../translate.js'

const ACCEPT_ID = 'accept-as-participant-button'
const REJECT_ID = 'reject-involved-button-'
const PARTICIPATIONS_GROUP = 'participations'
const REQUESTERS_GROUP = 'requests'
const EMPTY_TEMPLATE = ''

const LABELS = {
  [REQUESTERS_GROUP]: 'interested',
  [PARTICIPATIONS_GROUP]: 'participants'
}

export class Involved {
  constructor(retrieveInvolveds, groupName, collapsed) {
    this.retrieveInvolveds = retrieveInvolveds
    this.collapsed = collapsed
    this.groupName = groupName
  }

  template({ hasToShow }) {
    let templates = EMPTY_TEMPLATE
    if (!hasToShow) { return templates }
    if (!this.hasInvolved()) { return this.thereAreNoMessage() }

    const group = this.involvedOrdered()
    group.forEach((involved, index) => {

      if (involved.isRejected) {
        templates += this.rejectedTemplate()
      } else {
        templates += this.involvedTemplate(involved, index)
      }
    })

    return templates
  }

  //private

  involvedOrdered() {
    return [...this.retrieveInvolveds()].reverse()
  }

  rejectedTemplate() {
    return `
      <div class="box has-text-centered has-text-weight-bold">
        <span class="is-seize-5">${translate('participantWasRejected')}<span>
      </div>
    `
  }

  involvedTemplate(involved, index) {
    const id = `${this.groupName}-${index}`
    const avatar = "images/128x128.jpg"
    const userName = "Zero Live"
    const user = "@zerolive"

    return `
      <div class="box">
        <article class="media">
          <div class="media-left">
            <figure class="image is-64x64">
              <img src="${avatar}" alt="Avatar">
            </figure>
          </div>
          <div class="media-content">
            <div class="content">
              <p>
                <strong>${userName}</strong> <small>${user}</small> ${this.involvedButtons(involved.id)}
                <br id="${id}">
                ${involved.information}
              </p>
            </div>
          </div>
          <div class="media-rigth">
            ${this.collapseButton(index, involved.id)}
          </div>
        </article>
        ${this.comments(involved)}
        ${this.newComment(index, involved.id)}
      </div>
    `
  }

  collapseButton(index, involvedId) {
    let icon = "fas fa-angle-double-up"
    if (this.isCollapsed(involvedId)) { icon = "fas fa-angle-double-down" }

    return `
      <button class"button" id="${index}-collapse">
        <i class="${icon}"></i>
      </button>
    `
  }

  comments(involved) {
    const comments = involved.comments || []
    const templates = []
    if (this.isCollapsed(involved.id)) { return '' }

    if (comments.length > 0) {
      comments.forEach((comment) => {
        templates.push(`
          <div class="box">
            ${comment.comment}
          </div>
        `)
      })
    }

    return templates.join('')
  }

  newComment(index, involvedId) {
    if (this.isCollapsed(involvedId)) { return '' }

    return `
      <div class="box">
        <article class="media">
          <div class="media-content">
            <div class="content">
              <div class="field">
                <div class="control">
                  <textarea id="${index}-comment-input" class="textarea" placeholder="${translate('writeYourComment')}"></textarea>
                  <button class="button is-info" id="${index}-comment-button">${translate('comment')}</button>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    `
  }

  hasInvolved() {
    return (this.retrieveInvolveds().length > 0)
  }

  involvedButtons(id) {
    let buttons = ''

    if (this.isRequestersGroup()) { buttons = this.acceptButton(id) }
    if (this.isParticipantGroup()) { buttons = this.rejectButton(id) }

    return buttons
  }

  acceptButton(id) {
    return `<button class="button is-primary is-small" id="${ACCEPT_ID + id}">${translate('accept')}</button>`
  }

  rejectButton(id) {
    return `<button class="button is-danger is-small" id="${REJECT_ID + id}">${translate('reject')}</button>`
  }

  isRequestersGroup() {
    return (this.groupName === REQUESTERS_GROUP)
  }

  isParticipantGroup() {
    return (this.groupName === PARTICIPATIONS_GROUP)
  }

  isCollapsed(involvedId) {
    return this.collapsed.includes(involvedId)
  }

  thereAreNoMessage() {
    const group = translate(LABELS[this.groupName])
    const text = translate('thereAreNot') + ' ' + group
    return `
      <div class="box title has-text-grey-light has-text-centered	">
        ${text}
      </div>
    `
  }
}
