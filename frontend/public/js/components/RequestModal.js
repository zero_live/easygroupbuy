import { translate } from '../translate.js'
import { Component } from './Component.js'

const REQUIREMENTS_ID = 'requirements'

export class RequestModal extends Component {
  constructor(data) {
    super('request-modal')

    this.hasToHide = true
    this.buyGroup = data.buyGroup
    this.requirements = data.requirements
    this.requestToJoin = data.requestToJoin
    this.disableRequest = true
    this.requesterInformation = ''

    this.show = this.show.bind(this)
  }

  show() {
    this.hasToHide = false

    this.draw()
    this.focusRequirements()
  }

  hide() {
    this.hasToHide = true

    this.draw()
  }

  request() {
    this.requestToJoin(this.requesterInformation)
    this.hide()
  }

  //private

  template() {
    if (this.hasToHide) { return '' }

    return `
      <div class="modal is-active">
        <div class="modal-background"></div>
        <div class="modal-card">
          <header class="modal-card-head">
            <p class="modal-card-title">${translate('requestJoin')}</p>
            <button class="delete" aria-label="close" id="close-request-modal"></button>
          </header>
          <section class="modal-card-body">
            ${this.requirements}
            <label class="label">
              ${translate('requirementAreRequired')}
              <span class="has-text-danger has-text-weight-bold"> *</span>
            </label>
            <textarea class="textarea" id="${REQUIREMENTS_ID}"></textarea>
          </section>
          <footer class="modal-card-foot">
            ${this.requestButton()}
            <button class="button" id="cancel-request-modal">${translate('cancel')}</button>
          </footer>
        </div>
      </div>
    `
  }

  requestButton() {
    let disabled = 'disabled'

    if (this.hasRequirementsFilled()) { disabled = '' }

    return `<button id="request-button" class="button is-success" ${disabled}>${translate('request')}</button>`
  }

  addCallbacks() {
    this.addOnClick('close-request-modal', this.hide.bind(this))
    this.addOnClick('cancel-request-modal', this.hide.bind(this))
    this.addOnClick('request-button', this.request.bind(this))
    this.addEventListener('input', REQUIREMENTS_ID, this.updateRequesterInformation.bind(this))
  }

  updateRequesterInformation(event) {
    const value = event.target.value

    this.requesterInformation = value

    this.draw()
    this.prepareTextAreaRequirements(value)
  }

  prepareTextAreaRequirements(value) {
    const field = this.getElementById(REQUIREMENTS_ID)

    field.value = value
    field.focus()
  }

  focusRequirements() {
    this.prepareTextAreaRequirements('')
  }

  hasRequirementsFilled() {
    return (this.requesterInformation !== '')
  }
}
