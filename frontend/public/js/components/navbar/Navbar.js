import { translate } from '../../translate.js'
import { Component } from '../Component.js'
import { Loging } from './Login.js'

export class Navbar extends Component {
  constructor(data) {
    super('navbar')

    this.data = data
  }

  //private

  template() {
    return `
      <div class="hero-head">
        <nav class="navbar">
          <div class="container custom-hero-container">
            <div class="navbar-brand">
              <div class="navbar-item has-text-white">
                <i class="fas fa-3x fa-shopping-cart"></i>
                <a class="is-size-3 has-text-white" href="/">${translate('ebg')}</a>
              </div>
            </div>
            <div class="navbar-menu">
              <div class="navbar-end">
                <span class="navbar-item">
                  ${this.loginButton()}
                </span>
              </div>
            </div>
          </div>
        </nav>
      </div>
    `
  }

  loginButton() {
    return new Loging(this.data).template()
  }
}
