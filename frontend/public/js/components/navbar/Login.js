import { translate } from "../../translate.js"

export class Loging {
  constructor(data) {
    this.data = data
  }

  template() {
    if (this.data.user.isLogged()) { return this.avatar() }

    return this.loginButton()
  }

  loginButton() {
    return `
      <a class="button is-primary is-inverted" href="${this.data.user.loginUrl()}">
        <span class="icon">
          <i class="fas fa-user"></i>
        </span>
        <span>${translate('login')}</span>
      </a>
    `
  }

  avatar() {
    return `
      <a class="button is-primary is-inverted">
        <span class="icon">
          <figure class="image image is-24x24">
            <img class="is-rounded" src="${this.data.user.image()}">
          </figure>
        </span>
        <span>${this.data.user.name()}</span>
      </a>
    `
  }
}
