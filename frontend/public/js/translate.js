
export const translate = (label) => {
  const translations = {
    comment: 'Comentar',
    ebg: 'Easy Buy Group',
    buyGroup: 'Buy Group',
    description: 'Descripción',
    request: 'Solicitar',
    requiredInfo: 'Información necesaria para participar',
    requestJoin: 'Solicitar participar',
    createBuyGroup: 'Crear BuyGroup',
    title: 'Titulo',
    reject: 'Expulsar',
    participantWasRejected: 'Este participante a sido expulsado',
    maximumParticipants: 'Máxima cantidad de participantes',
    finalizationDateToJoin: 'Fecha limite para apuntarse',
    done: 'Terminar',
    finalized: 'Terminado!',
    create: 'Crear',
    cancel: 'Cancelar',
    missingInfo: 'Informatión requerida',
    buyGroupCreated: 'Buy Group creado!',
    requirementAreRequired: 'El organizador requiere que facilites la información descrita',
    join: 'Unirse',
    interested: 'Interesados',
    participants: 'Participantes',
    accept: 'Aceptar',
    thereAreNot: 'No hay',
    login: 'Login',
    writeYourComment: 'Escribe tu comentario',
  }
  const translation = translations[label]
  if (!translation) { throw 'Missing label: ' + label }

  return translations[label]
}
