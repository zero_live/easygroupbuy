import { BuyGroupDetail } from './components/buyGroupDetail/BuyGroupDetail.js'
import { BuyGroupModal } from './components/BuyGroupModal.js'
import { RequestModal } from './components/RequestModal.js'
import { Navbar } from './components/navbar/Navbar.js'
import { BuyGroupList } from './BuyGroupList.js'
import { Actions } from './domain/Actions.js'
import { Collection } from './Collection.js'
import { User } from './domain/User.js'

export class EasyGroupBuyPage {
  static draw() {
    const collection = new Collection(initialBuyGroups())
    const actions = new Actions(collection)
    const page = new EasyGroupBuyPage(actions, User)

    page.draw()
  }

  constructor(actions, user) {
    this.document = document

    this.actions = actions
    this.user = user
    this.buyGroup = this.defaultSelectedBuyGroup()
  }

  draw() {
    new Navbar({ user: this.user }).draw()
    const requestModal = new RequestModal({
      requirements: this.buyGroup.requirements,
      requestToJoin: this.requestToJoin.bind(this)
    })
    requestModal.draw()
    const newBuyGroupModal = new BuyGroupModal({
      create: this.actions.createBuyGroups,
      reload: this.redraw.bind(this),
      selectBuyGroup: this.selectBuyGroup.bind(this),
      user: this.user,
    })
    newBuyGroupModal.draw()

    new BuyGroupList({
      buyGroups: this.buyGroups(),
      selected: this.buyGroup,
      selectBuyGroup: this.selectBuyGroup.bind(this),
      newBuyGroup: newBuyGroupModal.show,
    }).draw()
    const editBuyGroupModal = new BuyGroupModal({
      create: this.actions.updateBuyGroup,
      buyGroup: this.buyGroup,
      reload: this.redraw.bind(this),
      selectBuyGroup: this.selectBuyGroup.bind(this),
      user: this.user,
    })
    editBuyGroupModal.draw()

    new BuyGroupDetail({
      buyGroup: this.buyGroup,
      requestToJoin: requestModal.show,
      retrieveRequests: this.actions.retrieveRequests,
      becomeParticipation: this.actions.becomeParticipation,
      retrieveParticipations: this.actions.retrieveParticipations,
      edit: editBuyGroupModal.show,
      rejectParticipant: this.actions.rejectInvolved,
      comment: this.actions.comment,
      reload: this.redraw.bind(this)
    }).draw()
  }

  redraw() {
    this.draw()
  }

  //private

  requestToJoin(information) {
    this.actions.requestToJoin(this.buyGroup.id, { information: information })

    this.draw()
  }

  buyGroups() {
    return this.actions.retrieveBuyGroups()
  }

  defaultSelectedBuyGroup() {
    const buyGroups = this.buyGroups()
    const newestPosition = buyGroups.length - 1

    return buyGroups[newestPosition]
  }

  selectBuyGroup(buyGroup) {
    this.buyGroup = buyGroup

    this.draw()
  }
}

function initialBuyGroups() {
  return [
    {
      id: 1,
      title: "Lluis' Group",
      currentParticipants: 1,
      maximumParticipants: '10',
      finalizationDate: '2222-01-01',
      status: 'OPEN',
      description: "Lucas ipsum dolor sit amet oppo medon opress hoojib naberrie zev depa gricho klatooinian ackbar. Clawdite binks oppo karrde mayagil yavin tiin. Theelin mas selkath muun yuzzem darth togruta. Bardan baba opress ailyn ima-gun. Di yavin ugnaught nien kamino gorog tchuukthai lorth kwi. Mon kasan palpatine jade wicket. Coruscant mustafar tion t'landa organa saffa c'baoth solo antilles. Hutt dunwell shimrra evocii max. R5-d4 thisspias cad mirta firrerreo atrivis gricho. Md-5 jagged sy togorian rhen aurra. Mayagil jubnuk coruscant thennqora verpine karrde amedda dagobah ferroans. Gwurran iktotchi galen lando fel ruurian. Xappyh tion wol rex nute vor moore. Elomin gand grievous hissa mirax firmus c-3p0 teneniel. Ka himoran moff pa'lowick cracken sneevel jodo x'ting gungan. Jar lepi tenel bothan shadda. Rodian joh rakata chirrpa bossk gorith olié. Aqualish ralter besalisk nass san. Maarek raa tagge anx besalisk skywalker vivenda leia. Porkins kota whill shaddaa bertroff calamari ubese nass. Qui-gon lars fel k-3po fortuna kyle saurin ord nikto. Mandalore darth sal-solo naboo daala toydarian polis paaerduag. Vao endocott tharin sal-solo t'landa. Han terrik cade naberrie zekk zuckuss bimm tapani gungan. Solo jabiimas vratix tof secura boz felucia. Ozzel neti drall doldur yané rahn caedus kenobi mirta. Su jabba mantell panaka firrerreo solo cadavine. Xanatos nadon yuuzhan h'nemthean gungan katarn. Dalonbian ric tapani grodin nute pa'lowick utapau. Wicket dunhausen vulptereen grievous illum dalonbian endocott grodin dooku. Reach defel hutt borsk finis epicanthix boba gordin adi. Boltrunians timoliini caamasi gonk binks solo maarek. Rukh letaki nar ors. Jade malastare thul cornelius mod shi'ido solo caamasi yaka. Himoran vagaari ogemite cody cade darth whill. Tono ysanne -1b cal qel-droma keshiri kal x1. Hobbie rakata droch rakata calamari. Hutta endocott warrick kor-uj ewok darth dooku. Sy lytton caedus nelvaanian lando mirax. Hutt sunrider zev hypori jamillia roos jax. Emtrey -lom baba palpatine malastare greedo airen firmus moore. Jobal jinn celegian shimrra. Baba porkins anakin davin terrik polis raynar. Tusken raider organa san vurk tarasin croke geonosis zeltron. Sarn ruwee rattataki mohc bothan greedo tchuukthai. Bith dexter dantooine massassi. Xexto cal tarasin tchuukthai y'bith katarn conan. Koon defel ogemite shadda weequay kendal xanatos. Epicanthix moff elom garindan balosar bossk tsavong thistleborn. Sykes hypori oswaft isolder whaladon mandalorians colton darth. Mas yowza mara ithorian ord hoojib antemeridian anzati. Greedo binks boba cracken hissa sluis. Derlin shaak kiffar pa'lowick md-5 duro.",
      requirements: "Lucas ipsum dolor sit amet alderaan han owen moff organa mon palpatine solo antilles jango. Twi'lek luuke alderaan qui-gon k-3po dagobah wedge moff organa. Darth mon organa alderaan yoda organa jango luke. Dantooine moff ben lando. Moff skywalker moff fett calrissian kenobi maul dooku. Skywalker watto wookiee obi-wan hutt skywalker jango. Skywalker mon greedo greedo greedo dantooine darth. Organa lars dagobah maul palpatine skywalker sith. Coruscant calrissian hutt fett solo dooku. Kessel grievous maul calamari c-3p0 darth.",
    },
    {
      id: 2,
      title: "Mario's Group",
      currentParticipants: 2,
      maximumParticipants: '10',
      finalizationDate: '2222-02-02',
      status: 'OPEN',
      description: "Lucas ipsum dolor sit amet oppo medon opress hoojib naberrie zev depa gricho klatooinian ackbar. Clawdite binks oppo karrde mayagil yavin tiin. Theelin mas selkath muun yuzzem darth togruta. Bardan baba opress ailyn ima-gun. Di yavin ugnaught nien kamino gorog tchuukthai lorth kwi. Mon kasan palpatine jade wicket. Coruscant mustafar tion t'landa organa saffa c'baoth solo antilles. Hutt dunwell shimrra evocii max. R5-d4 thisspias cad mirta firrerreo atrivis gricho. Md-5 jagged sy togorian rhen aurra. Mayagil jubnuk coruscant thennqora verpine karrde amedda dagobah ferroans. Gwurran iktotchi galen lando fel ruurian. Xappyh tion wol rex nute vor moore. Elomin gand grievous hissa mirax firmus c-3p0 teneniel. Ka himoran moff pa'lowick cracken sneevel jodo x'ting gungan. Jar lepi tenel bothan shadda. Rodian joh rakata chirrpa bossk gorith olié. Aqualish ralter besalisk nass san. Maarek raa tagge anx besalisk skywalker vivenda leia. Porkins kota whill shaddaa bertroff calamari ubese nass. Qui-gon lars fel k-3po fortuna kyle saurin ord nikto. Mandalore darth sal-solo naboo daala toydarian polis paaerduag. Vao endocott tharin sal-solo t'landa. Han terrik cade naberrie zekk zuckuss bimm tapani gungan. Solo jabiimas vratix tof secura boz felucia. Ozzel neti drall doldur yané rahn caedus kenobi mirta. Su jabba mantell panaka firrerreo solo cadavine. Xanatos nadon yuuzhan h'nemthean gungan katarn. Dalonbian ric tapani grodin nute pa'lowick utapau. Wicket dunhausen vulptereen grievous illum dalonbian endocott grodin dooku. Reach defel hutt borsk finis epicanthix boba gordin adi. Boltrunians timoliini caamasi gonk binks solo maarek. Rukh letaki nar ors. Jade malastare thul cornelius mod shi'ido solo caamasi yaka. Himoran vagaari ogemite cody cade darth whill. Tono ysanne -1b cal qel-droma keshiri kal x1. Hobbie rakata droch rakata calamari. Hutta endocott warrick kor-uj ewok darth dooku. Sy lytton caedus nelvaanian lando mirax. Hutt sunrider zev hypori jamillia roos jax. Emtrey -lom baba palpatine malastare greedo airen firmus moore. Jobal jinn celegian shimrra. Baba porkins anakin davin terrik polis raynar. Tusken raider organa san vurk tarasin croke geonosis zeltron. Sarn ruwee rattataki mohc bothan greedo tchuukthai. Bith dexter dantooine massassi. Xexto cal tarasin tchuukthai y'bith katarn conan. Koon defel ogemite shadda weequay kendal xanatos. Epicanthix moff elom garindan balosar bossk tsavong thistleborn. Sykes hypori oswaft isolder whaladon mandalorians colton darth. Mas yowza mara ithorian ord hoojib antemeridian anzati. Greedo binks boba cracken hissa sluis. Derlin shaak kiffar pa'lowick md-5 duro.",
      requirements: "Lucas ipsum dolor sit amet alderaan han owen moff organa mon palpatine solo antilles jango. Twi'lek luuke alderaan qui-gon k-3po dagobah wedge moff organa. Darth mon organa alderaan yoda organa jango luke. Dantooine moff ben lando. Moff skywalker moff fett calrissian kenobi maul dooku. Skywalker watto wookiee obi-wan hutt skywalker jango. Skywalker mon greedo greedo greedo dantooine darth. Organa lars dagobah maul palpatine skywalker sith. Coruscant calrissian hutt fett solo dooku. Kessel grievous maul calamari c-3p0 darth.",
    },
    {
      id: 3,
      title: "Jaume's Group",
      currentParticipants: 3,
      maximumParticipants: '10',
      finalizationDate: '2222-03-03',
      status: 'OPEN',
      description: "Lucas ipsum dolor sit amet oppo medon opress hoojib naberrie zev depa gricho klatooinian ackbar. Clawdite binks oppo karrde mayagil yavin tiin. Theelin mas selkath muun yuzzem darth togruta. Bardan baba opress ailyn ima-gun. Di yavin ugnaught nien kamino gorog tchuukthai lorth kwi. Mon kasan palpatine jade wicket. Coruscant mustafar tion t'landa organa saffa c'baoth solo antilles. Hutt dunwell shimrra evocii max. R5-d4 thisspias cad mirta firrerreo atrivis gricho. Md-5 jagged sy togorian rhen aurra. Mayagil jubnuk coruscant thennqora verpine karrde amedda dagobah ferroans. Gwurran iktotchi galen lando fel ruurian. Xappyh tion wol rex nute vor moore. Elomin gand grievous hissa mirax firmus c-3p0 teneniel. Ka himoran moff pa'lowick cracken sneevel jodo x'ting gungan. Jar lepi tenel bothan shadda. Rodian joh rakata chirrpa bossk gorith olié. Aqualish ralter besalisk nass san. Maarek raa tagge anx besalisk skywalker vivenda leia. Porkins kota whill shaddaa bertroff calamari ubese nass. Qui-gon lars fel k-3po fortuna kyle saurin ord nikto. Mandalore darth sal-solo naboo daala toydarian polis paaerduag. Vao endocott tharin sal-solo t'landa. Han terrik cade naberrie zekk zuckuss bimm tapani gungan. Solo jabiimas vratix tof secura boz felucia. Ozzel neti drall doldur yané rahn caedus kenobi mirta. Su jabba mantell panaka firrerreo solo cadavine. Xanatos nadon yuuzhan h'nemthean gungan katarn. Dalonbian ric tapani grodin nute pa'lowick utapau. Wicket dunhausen vulptereen grievous illum dalonbian endocott grodin dooku. Reach defel hutt borsk finis epicanthix boba gordin adi. Boltrunians timoliini caamasi gonk binks solo maarek. Rukh letaki nar ors. Jade malastare thul cornelius mod shi'ido solo caamasi yaka. Himoran vagaari ogemite cody cade darth whill. Tono ysanne -1b cal qel-droma keshiri kal x1. Hobbie rakata droch rakata calamari. Hutta endocott warrick kor-uj ewok darth dooku. Sy lytton caedus nelvaanian lando mirax. Hutt sunrider zev hypori jamillia roos jax. Emtrey -lom baba palpatine malastare greedo airen firmus moore. Jobal jinn celegian shimrra. Baba porkins anakin davin terrik polis raynar. Tusken raider organa san vurk tarasin croke geonosis zeltron. Sarn ruwee rattataki mohc bothan greedo tchuukthai. Bith dexter dantooine massassi. Xexto cal tarasin tchuukthai y'bith katarn conan. Koon defel ogemite shadda weequay kendal xanatos. Epicanthix moff elom garindan balosar bossk tsavong thistleborn. Sykes hypori oswaft isolder whaladon mandalorians colton darth. Mas yowza mara ithorian ord hoojib antemeridian anzati. Greedo binks boba cracken hissa sluis. Derlin shaak kiffar pa'lowick md-5 duro.",
      requirements: "Lucas ipsum dolor sit amet alderaan han owen moff organa mon palpatine solo antilles jango. Twi'lek luuke alderaan qui-gon k-3po dagobah wedge moff organa. Darth mon organa alderaan yoda organa jango luke. Dantooine moff ben lando. Moff skywalker moff fett calrissian kenobi maul dooku. Skywalker watto wookiee obi-wan hutt skywalker jango. Skywalker mon greedo greedo greedo dantooine darth. Organa lars dagobah maul palpatine skywalker sith. Coruscant calrissian hutt fett solo dooku. Kessel grievous maul calamari c-3p0 darth.",
    },
    {
      id: 4,
      title: "Vilva's Group",
      currentParticipants: 4,
      maximumParticipants: '10',
      finalizationDate: '2222-04-04',
      status: 'OPEN',
      description: "Lucas ipsum dolor sit amet oppo medon opress hoojib naberrie zev depa gricho klatooinian ackbar. Clawdite binks oppo karrde mayagil yavin tiin. Theelin mas selkath muun yuzzem darth togruta. Bardan baba opress ailyn ima-gun. Di yavin ugnaught nien kamino gorog tchuukthai lorth kwi. Mon kasan palpatine jade wicket. Coruscant mustafar tion t'landa organa saffa c'baoth solo antilles. Hutt dunwell shimrra evocii max. R5-d4 thisspias cad mirta firrerreo atrivis gricho. Md-5 jagged sy togorian rhen aurra. Mayagil jubnuk coruscant thennqora verpine karrde amedda dagobah ferroans. Gwurran iktotchi galen lando fel ruurian. Xappyh tion wol rex nute vor moore. Elomin gand grievous hissa mirax firmus c-3p0 teneniel. Ka himoran moff pa'lowick cracken sneevel jodo x'ting gungan. Jar lepi tenel bothan shadda. Rodian joh rakata chirrpa bossk gorith olié. Aqualish ralter besalisk nass san. Maarek raa tagge anx besalisk skywalker vivenda leia. Porkins kota whill shaddaa bertroff calamari ubese nass. Qui-gon lars fel k-3po fortuna kyle saurin ord nikto. Mandalore darth sal-solo naboo daala toydarian polis paaerduag. Vao endocott tharin sal-solo t'landa. Han terrik cade naberrie zekk zuckuss bimm tapani gungan. Solo jabiimas vratix tof secura boz felucia. Ozzel neti drall doldur yané rahn caedus kenobi mirta. Su jabba mantell panaka firrerreo solo cadavine. Xanatos nadon yuuzhan h'nemthean gungan katarn. Dalonbian ric tapani grodin nute pa'lowick utapau. Wicket dunhausen vulptereen grievous illum dalonbian endocott grodin dooku. Reach defel hutt borsk finis epicanthix boba gordin adi. Boltrunians timoliini caamasi gonk binks solo maarek. Rukh letaki nar ors. Jade malastare thul cornelius mod shi'ido solo caamasi yaka. Himoran vagaari ogemite cody cade darth whill. Tono ysanne -1b cal qel-droma keshiri kal x1. Hobbie rakata droch rakata calamari. Hutta endocott warrick kor-uj ewok darth dooku. Sy lytton caedus nelvaanian lando mirax. Hutt sunrider zev hypori jamillia roos jax. Emtrey -lom baba palpatine malastare greedo airen firmus moore. Jobal jinn celegian shimrra. Baba porkins anakin davin terrik polis raynar. Tusken raider organa san vurk tarasin croke geonosis zeltron. Sarn ruwee rattataki mohc bothan greedo tchuukthai. Bith dexter dantooine massassi. Xexto cal tarasin tchuukthai y'bith katarn conan. Koon defel ogemite shadda weequay kendal xanatos. Epicanthix moff elom garindan balosar bossk tsavong thistleborn. Sykes hypori oswaft isolder whaladon mandalorians colton darth. Mas yowza mara ithorian ord hoojib antemeridian anzati. Greedo binks boba cracken hissa sluis. Derlin shaak kiffar pa'lowick md-5 duro.",
      requirements: "Lucas ipsum dolor sit amet alderaan han owen moff organa mon palpatine solo antilles jango. Twi'lek luuke alderaan qui-gon k-3po dagobah wedge moff organa. Darth mon organa alderaan yoda organa jango luke. Dantooine moff ben lando. Moff skywalker moff fett calrissian kenobi maul dooku. Skywalker watto wookiee obi-wan hutt skywalker jango. Skywalker mon greedo greedo greedo dantooine darth. Organa lars dagobah maul palpatine skywalker sith. Coruscant calrissian hutt fett solo dooku. Kessel grievous maul calamari c-3p0 darth.",
    },
    {
      id: 5,
      title: "Compra conjunta Calendarios de León 2020",
      currentParticipants: 5,
      maximumParticipants: '10',
      finalizationDate: '2222-05-05',
      status: 'OPEN',
      description: "<p>Bueno, pues un año mas (y ya son 10 con éste, como pasa el tiempo  :-\) voy a encargarme del tema de los calendarios, este año al igual que el pasado el 10 de diciembre es la fecha máxima de ingreso del importe en la cuenta de Furgovw pero con la diferencia de que, como ya está operativa la tienda, en vez de hacerme el ingreso a mi los apuntados habréis de hacer el ingreso en la cuenta del foro como máximo dicho día 9 de diciembre, pues el 10 se cerrarán automáticamente los pedidos y el distribuidor me enviará a mi (como encargado de la compra conjunta) los calendarios, si alguien se quedara fuera aún tendría la posibilidad de comprar individualmente el calendario en la tienda, pero sin el beneficio de gastos de envío cero que tendríamos en la compra conjunta (siempre y cuando sobrepasáramos los 10 calendarios, cosa que siempre ha sucedido holgadamente).</p><p>A pesar de todo si alguno por causa justificada (entiendo que hacer una transferencia on line hoy en día no lo es, porque ya lo saben hacer hasta los chavales de 15 años) no pudiera/no supiera hacer el pago en la cuenta del foro aceptaré que me lo entregue a mi (en mano lógicamente, si es para hacerme una transferencia para eso estaría la cuenta del foro), y me ofrezco a ayudar al que no se aclare pero, por favor, no vayamos a dejarlo para última hora que además este año, con el jaleo de la tienda, los plazos van bastante apurados.</p><p>Lo dicho, la dinámica de la compra conjunta será:</p><p>1.- Apuntarse como siempre en este hilo a la compra indicando el número de unidades que se quiere.</p><p>2.- En el momento en que superemos las 10 unidades se nos abrirá la opción de <b>envió gratuito compra conjunta</b> en la tienda, y a partir de ese momento y hasta el día 9 de diciembre cada uno deberá pagarse sus calendarios indicando su Nick y el número de unidades.</p><p>3.- El día 11 de diciembre saldrán los calendarios y en cuanto los reciba avisaré por aquí y podremos irlos entregando, aunque organizaremos también la típica comida navideña para entrega a los que quieran/puedan asistir.</p><br><p><h3>FORMATO DE LOS CALENDARIOS</h3></p><br><p>Para el que no los conozca (si es que hay alguien  ;D) el pack incluye un calendario de Pared Formato DIN A-4, compuesto por 7 hojas (14 Paginas) impresas a 4+4 colores + Barniz sobre papel couche brillo de 200 grs. Encuadernado en Wire-O (espiral) con colgador al 210 mms. mas un calendario de sobremesa.</p><p>El importe por calendario es de 7€, en cuanto a los gastos de envío caso de superar las 10 unidades serían gratis (siempre hemos superado esa cifra holgadamente, espero que este año también) , en caso contrario habría que abonar los gastos de envío (que dependerían según fueran por correo ordinario o mensajería) que nos repartiríamos proporcionalmente todos los interesados.</p><p>Los interesados que se vayan apuntando indicando su nick y número de calendarios, y yo iré actualizando el listado de abajo en la medida de lo posible, a medida que me indiquéis que habéis hecho el ingreso os iré poniendo el nick en verde, y a medida que se entreguen los iré poniendo en color azul, haced lo posible para no dejarlo todo para última hora y a ver si podemos repartir los más posibles en una única ocasión (la comida navideña, por ejemplo) que así me evitáis mucho trabajo.</p><p>Mas datos aquí: https://www.furgovw.org/index.php?topic=353015.msg4483447</p>",
      requirements: "Aunque no es necesario para mi mejor control agradecería que los que fueran pagando en la cuenta XXXXXXXXXXXXXXXXXXX también lo dijeran en este hilo para ir controlando los pagos.",
    }
  ]
}
