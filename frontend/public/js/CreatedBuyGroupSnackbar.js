import { translate } from "./translate.js"

export class CreatedBuyGroupSnackbar {
  constructor() {
    this.show = this.show.bind(this)
    this.TIME_TO_HIDE = 3000

    this.element = document.getElementById('snackbar')
    this.element.innerHTML = translate('finalized')
  }

  show() {
    this.element.className = "show"

    setTimeout(this.hideAfterTime.bind(this), this.TIME_TO_HIDE)
  }

  hideAfterTime() {
    this.element.className = this.element.className.replace("show", "")
  }
}
