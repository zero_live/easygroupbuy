
export const participantsSummary = (buyGroup) => {
  const current = buyGroup.currentParticipants
  const maximum = buyGroup.maximumParticipants
  let templateBase = `<i class="fas fa-users"></i>${current}`

  if (isDefined(maximum)) { templateBase += `/${maximum}` }

  return templateBase
}

const isDefined = (number) => {
  return !isNaN(number)
}
