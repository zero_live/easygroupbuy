import { participantsSummary } from './participantsSummary.js'
import { toLocaleDate } from './toLocaleDate.js'

const HIGHLIST_CSS = ' has-background-grey-lighter'

export const buyGroupListItem = (buyGroup) => {
  const title = buyGroup.title
  const finalizationDate = toLocaleDate(buyGroup.finalizationDate)
  const status = buyGroup.status

  const activeStyles = (isActive) => {
    let css = 'box custom-as-link'

    if (isActive) { css += HIGHLIST_CSS }

    return css
  }

  return `
    <div id="${buyGroup.htmlId}" class="${activeStyles(buyGroup.isActive)}">
      <div class="level" id="buy-group-title">
        <div class="level-item">${title}</div>
      </div>
      <div class="level" id="buy-group-summary">
        <span class="level-item" id="buy-group-participants">
          ${participantsSummary(buyGroup)}
        </span>
        <span class="level-item" id="buy-group-finalization-date">
          ${finalizationDate}
        </span>
        <span class="level-item" id="buy-group-status">
          ${status}
        </span>
      </div>
    </div>
  `
}
