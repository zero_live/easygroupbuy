/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./spec/support/BuyGroupSamples.js":
/*!*****************************************!*\
  !*** ./spec/support/BuyGroupSamples.js ***!
  \*****************************************/
/*! exports provided: BuyGroupSamples */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BuyGroupSamples\", function() { return BuyGroupSamples; });\n\nclass BuyGroupSamples {\n  static many() {\n    return [\n      {\n        title: \"Lluis' Group\",\n        currentParticipants: '1',\n        maximumParticipants: '10',\n        finalizationDate: '01/01/2222',\n        status: 'OPEN',\n      },\n      {\n        title: \"Mario's Group\",\n        currentParticipants: '2',\n        maximumParticipants: '10',\n        finalizationDate: '02/02/2222',\n        status: 'OPEN',\n      },\n      {\n        title: \"Jaume's Group\",\n        currentParticipants: '3',\n        maximumParticipants: '10',\n        finalizationDate: '03/03/2222',\n        status: 'OPEN',\n      },\n      {\n        title: \"Vilva's Group\",\n        currentParticipants: '4',\n        maximumParticipants: '10',\n        finalizationDate: '04/04/2222',\n        status: 'OPEN',\n      },\n      this.some(),\n    ]\n  }\n\n  static some() {\n    return {\n      title: \"Zero's Group\",\n      currentParticipants: '5',\n      maximumParticipants: '10',\n      finalizationDate: '05/05/2222',\n      status: 'OPEN',\n    }\n  }\n}\n\n\n//# sourceURL=webpack:///./spec/support/BuyGroupSamples.js?");

/***/ }),

/***/ "./src/BuyGroupList.js":
/*!*****************************!*\
  !*** ./src/BuyGroupList.js ***!
  \*****************************/
/*! exports provided: BuyGroupList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BuyGroupList\", function() { return BuyGroupList; });\n/* harmony import */ var _buyGroupListItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./buyGroupListItem */ \"./src/buyGroupListItem.js\");\n\n\nclass BuyGroupList {\n  static in(id) {\n    const list = new BuyGroupList(document, id)\n\n    return list\n  }\n\n  constructor(document, id) {\n    this.element = document.getElementById(id)\n  }\n\n  draw({ buyGroups=[] }) {\n    const decorators = this.decorate(buyGroups)\n\n    this.element.innerHTML = this.toHtml(decorators)\n  }\n\n  //private\n\n  decorate(buyGroups) {\n    const decorators = buyGroups.map((buyGroup, index) => {\n      const decorated = this.decorateOne(buyGroup, index)\n\n      return Object(_buyGroupListItem__WEBPACK_IMPORTED_MODULE_0__[\"buyGroupListItem\"])(decorated)\n    })\n\n    return decorators\n  }\n\n  decorateOne(buyGroup, index) {\n    const decorated = buyGroup\n\n    decorated.isActive = this.isActive(index)\n\n    return decorated\n  }\n\n  toHtml(decorators) {\n    return decorators.join('')\n  }\n\n  isActive(index) {\n    return (index === 0)\n  }\n}\n\n\n//# sourceURL=webpack:///./src/BuyGroupList.js?");

/***/ }),

/***/ "./src/buyGroupListItem.js":
/*!*********************************!*\
  !*** ./src/buyGroupListItem.js ***!
  \*********************************/
/*! exports provided: buyGroupListItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"buyGroupListItem\", function() { return buyGroupListItem; });\n\nconst HIGHLIST_CSS = ' has-background-grey-lighter'\n\nconst buyGroupListItem = (buyGroup) => {\n  const title = buyGroup.title\n  const currentParticipants = buyGroup.currentParticipants\n  const maximumParticipants = buyGroup.maximumParticipants\n  const finalizationDate = buyGroup.finalizationDate\n  const status = buyGroup.status\n\n  const participants = currentParticipants + '/' + maximumParticipants\n  const activeStyles = (isActive) => {\n    let css = 'box'\n\n    if (isActive) { css += HIGHLIST_CSS }\n\n    return css\n  }\n\n  return `\n    <div class=\"${activeStyles(buyGroup.isActive)}\">\n      <div class=\"level\" id=\"buy-group-title\">\n        <div class=\"level-item\">${title}</div>\n      </div>\n      <div class=\"level\" id=\"buy-group-summary\">\n        <span class=\"level-item\" id=\"buy-group-participants\">\n          <i class=\"fas fa-users\"></i>${participants}\n        </span>\n        <span class=\"level-item\" id=\"buy-group-finalization-date\">\n          ${finalizationDate}\n        </span>\n        <span class=\"level-item\" id=\"buy-group-status\">\n          ${status}\n        </span>\n      </div>\n    </div>\n  `\n}\n\n\n//# sourceURL=webpack:///./src/buyGroupListItem.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _spec_support_BuyGroupSamples__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../spec/support/BuyGroupSamples */ \"./spec/support/BuyGroupSamples.js\");\n/* harmony import */ var _BuyGroupList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BuyGroupList */ \"./src/BuyGroupList.js\");\n\n\n\nconst buyGroups = _spec_support_BuyGroupSamples__WEBPACK_IMPORTED_MODULE_0__[\"BuyGroupSamples\"].many()\n\n_BuyGroupList__WEBPACK_IMPORTED_MODULE_1__[\"BuyGroupList\"].in('buy-group-list').draw({ buyGroups })\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });