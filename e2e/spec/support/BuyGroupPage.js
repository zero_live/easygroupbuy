
const REQUIRED_INFORMATION_FILLED = 'Some Required Information for fill'

export class BuyGroupPage {
  constructor() {
    cy.visit('/')
  }

  moveToLogin() {
    cy.get('#new-buy-group-button').click()

    return this
  }

  doLogin() {
    cy.get('#fake-login').click()
  }

  selectLastBuyGroup() {
    cy.get('#buyGroup5').click()

    return true
  }

  clickOnNewBuyGroup() {
    cy.get('#new-buy-group-button').click()
    cy.get('#fake-login').click()
    cy.get('#new-buy-group-button').click()
  }

  clickOnEditBuyGroup() {
    cy.get('#edit-buy-group-button').click()
    cy.get('#fake-login').click()
    cy.get('#edit-buy-group-button').click()
  }

  fillNewBuyGroup() {
    cy.get('#new-title').type('new-title')
    cy.get('#new-description').type('new-description')
    cy.get('#new-requirements').type('new-requirements')
  }

  fillTitle(text) {
    const titleInput = cy.get('#new-title')

    titleInput.clear()
    titleInput.type(text)
  }

  confirmCreation() {
    cy.get('#create-buy-group').click()
  }

  requestToJoin() {
    cy.get('#request-to-join-button').click()
  }

  fillRequirements() {
    cy.get('#requirements').type(REQUIRED_INFORMATION_FILLED)
  }

  request() {
    cy.get('#request-button').click()
  }

  createRequest() {
    this.requestToJoin()
    this.fillRequirements()
    this.request()
  }

  acceptRequest() {
    cy.get('#accept-as-participant-button0').click()
  }

  showParticipationList() {
    cy.get('#participations-tab').click()
  }

  isShowingParticipations() {
    cy.get('#participations-0')
    cy.contains('Zero Live')
    cy.contains('@zerolive')
    cy.contains(REQUIRED_INFORMATION_FILLED)

    return true
  }

  isShowingRequester() {
    cy.get('#accept-as-participant-button0')
    cy.contains('Zero Live')
    cy.contains('@zerolive')
    cy.contains(REQUIRED_INFORMATION_FILLED)

    return true
  }

  showsCreationFeedback() {
    cy.contains('Terminado!')

    return true
  }

  showsList() {
    cy.contains("Lluis' Group 1/10 1/1/2222 OPEN")
    cy.contains("Mario's Group 2/10 2/2/2222 OPEN")
    cy.contains("Jaume's Group 3/10 3/3/2222 OPEN")
    cy.contains("Vilva's Group 4/10 4/4/2222 OPEN")
    cy.contains("Compra conjunta Calendarios de León 2020 5/10 5/5/2222 OPEN")

    return true
  }

  showsLastDetail() {
    cy.get('#buyGroupDetail5')

    return true
  }

  showsNewBuyGroupInList() {
    cy.contains("new-title 0 --/--/-- OPEN")

    return true
  }

  showsNewBuyGroupDetail() {
    cy.contains('new-description')
    cy.contains('new-requirements')

    return true
  }

  includes(text) {
    cy.contains(text)

    return true
  }
}
