import { BuyGroupPage } from './support/BuyGroupPage'

describe('BuyGroupPage', () => {
  it('shows a list', () => {

    const page = new BuyGroupPage()

    expect(page.showsList()).to.eq(true)
  })

  it('shows any BuyGroup detail from list', () => {
    const page = new BuyGroupPage()

    page.selectLastBuyGroup()

    expect(page.showsLastDetail()).to.eq(true)
  })

  it('can create a new BuyGroup', () => {
    const page = new BuyGroupPage()
    page.clickOnNewBuyGroup()

    page.fillNewBuyGroup()
    page.confirmCreation()

    expect(page.showsCreationFeedback()).to.eq(true)
    expect(page.showsNewBuyGroupInList()).to.eq(true)
    expect(page.showsNewBuyGroupDetail()).to.eq(true)
  })

  it('requests to join to BuyGroup', () => {
    const page = new BuyGroupPage()

    page.requestToJoin()
    page.fillRequirements()
    page.request()

    expect(page.isShowingRequester()).to.eq(true)
  })

  it('accepts the request to join to BuyGroup', () => {
    const page = new BuyGroupPage()
    page.createRequest()

    page.acceptRequest()
    page.showParticipationList()

    expect(page.isShowingParticipations()).to.eq(true)
    expect(page.includes('6/10')).to.eq(true)
  })

  it('can edit a BuyGroup', () => {
    const newTitle = 'Edited title'
    const page = new BuyGroupPage()
    page.clickOnEditBuyGroup()

    page.fillTitle(newTitle)
    page.confirmCreation()

    expect(page.includes(newTitle)).to.eq(true)
  })

  it('can identify user', () => {
    const userName = 'Zero_Live'
    const page = new BuyGroupPage().moveToLogin()

    page.doLogin()

    expect(page.includes(userName)).to.eq(true)
  })
})
