# Easy Group Buy

## System requirements

- `docker-compose version 1.24.1,`
- `Docker version 19.03.5`

## Scripts

- Run the project `sh scripts/up`
- Run all tests `sh scripts/test`
- View project logs `sh scripts/logs`
- Stop the project `sh scripts/down`

## Use the project

- You have to open the url `http://localhost:3000/` with you browser.

## Code

You can find the `frontend` code at `frontend/public/js/` folder.

## Demo environment

- You can find a live demo in [https://egb-web.herokuapp.com](https://egb-web.herokuapp.com)
